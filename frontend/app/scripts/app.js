'use strict';

angular
  .module('degreeEvaluationsApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute',
    'degreeEvaluationsApp.degreeStructureServices',
    'degreeEvaluationsApp.services',
    'hops-directives',
    'ui.bootstrap'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/degree.html',
        controller: 'DegreeStructureController'
      })
      .when('/attach', {
        templateUrl: 'views/attach.html',
        controller: 'StudyRecordConroller'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
