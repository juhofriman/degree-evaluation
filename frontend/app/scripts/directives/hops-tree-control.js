(function ( angular ) {
    'use strict';
    // Based on tree control: http://ngmodules.org/modules/angular-tree-control
    angular.module( 'hops-directives', [] )
        .directive( 'hopsTreeControl', ['$compile', function( $compile ) {
            return {
                restrict: 'E',
                require: "hops-tree-control",
                transclude: true,
                scope: {
                    hopsModel: "=",
                    editMode: "="
                },
                controller: function( $scope ) {
                    
                    $scope.expandedNodes = {};
                    
                    $scope.nodeExpanded = function() {
                        //return $scope.expandedNodes[this.$id];
                        return true;
                    };
                    
                    $scope.nodeHasChildren = function(){
                        if(!this.node.children){
                            return false;
                        }
                        return this.node.children.length > 0;
                    };
                    
                    $scope.selectNodeHead = function() {
                        $scope.expandedNodes[this.$id] = !$scope.expandedNodes[this.$id];
                    };
                    
                    $scope.shouldShowNode = function() {
                        return $scope.editMode || this.node.selected == true;
                    };
                    
                    $scope.shouldDisplaySelect = function(){
                        if(!$scope.editMode ) {
                            return false;
                        }
                        if(this.node.grade){
                            return false;
                        }
                        return !this.node.mandatory;
                    }
     
                    $scope.select = function(){
                        $scope.$emit('selectionChanged', this.node);
                    }
                    
                    $scope.nodeExpandoClass = function() {
                        if($scope.expandedNodes[this.$id]){
                            return "glyphicon-chevron-down";
                        } else {
                            return "glyphicon-chevron-right";
                        }
                   };
             
                    var template =
                            '<ul>' +
                            '<li collapse="!shouldShowNode()" ng-repeat="node in node.children">' +
                            '<div  class="tree-label">' + 
                            //'<span  ng-click="selectNodeHead()" class="glyphicon" ng-class="nodeExpandoClass()" ng-show="nodeHasChildren()"></span>' +
                            '<span tree-transclude></span>' +
                            '</div>' +
                            '<div collapse="!nodeExpanded()">' +
                            '<treeitem></treeitem>' +
                            '</div>'
                            '</li>' +
                            '</ul>';
                            
                    return {
                        templateRoot: $compile(template),
                        templateChild: $compile(template)
                    }
                },
                compile: function(element, attrs, childTranscludeFn) {
                    return function ( scope, element, attrs, hopsModelCntr ) {
                        function updateNodeOnRootScope(newValue) {
                            if (angular.isArray(newValue)) {
                                scope.node = {};
                                scope.node['children'] = newValue;
                            }
                            else {
                                scope.node = newValue;
                            }
                        }
                        scope.$watch("hopsModel", updateNodeOnRootScope);
                        updateNodeOnRootScope(scope.hopsModel);

                        //Rendering template for a root node
                        hopsModelCntr.templateRoot( scope, function(clone) {
                            element.html('').append( clone );
                        });
                        // save the transclude function from compile (which is not bound to a scope as apposed to the one from link)
                        // we can fix this to work with the link transclude function with angular 1.2.6. as for angular 1.2.0 we need
                        // to keep using the compile function
                        scope.$treeTransclude = childTranscludeFn;
                    }
                }
            };
        }])
        .directive("treeitem", function() {
            return {
                restrict: 'E',
                require: "^hops-tree-control",
                link: function( scope, element, attrs, hopsModelCntr) {

                    // Rendering template for the current node
                    hopsModelCntr.templateChild(scope, function(clone) {
                        element.html('').append(clone);
                    });
                }
            }
        })
        .directive("treeTransclude", function() {
            return {
                link: function(scope, element, attrs, controller) {
                    scope.$treeTransclude(scope, function(clone) {
                        element.empty();
                        element.append(clone);
                    });
                }
            }
        });
})( angular );




