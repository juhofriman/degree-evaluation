
'use strict';

angular.module('degreeEvaluationsApp.services', [])
  .factory('studyRecordService', ['$resource', function($resource){
     var StudentResource = $resource('/student-api', {}, {'query':  {method:'GET', isArray:true}});
     var StudyRecordResource =  $resource('/student-api/:id', {}, {'save': {method: 'POST'}, 'getFor':  {method:'GET', isArray:true}});
     
     var IdeaResource =  $resource('/idea-api/:id', {}, {'query':  {method:'GET', isArray:true}});
     var StudyRecordService = {
        data: {
           allStudents: null,
           studyRecords: null,
           allIdeas: null
        },
        getAllStudents: function(){
            StudyRecordService.data.allStudents = StudentResource.query({});
        },
        getStudyRecords: function(studentId){
            StudyRecordService.data.studyRecords = StudyRecordResource.getFor({'id': studentId});
        },
        getIdeas: function(studentId){
            StudyRecordService.data.allIdeas = IdeaResource.query({});
        },
        currentStudyRecordsContain: function(idIdea){
            var contains = false;
            _.each(StudyRecordService.data.studyRecords, function(studyRecord){
                if(studyRecord.idIdea === idIdea){
                    contains = true;
                }
            });
            return contains;
        },
        save: function(studentId, evaluationDummy) {
            StudyRecordResource.save({'id': studentId}, evaluationDummy, function(){
                StudyRecordService.getStudyRecords(studentId);
            });
        }
     };
     StudyRecordService.getAllStudents();
     StudyRecordService.getIdeas();
     console.log(StudyRecordService.data.allIdeas);
     return StudyRecordService;
  }]);

