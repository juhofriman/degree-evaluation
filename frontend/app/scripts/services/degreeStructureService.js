
'use strict';

angular.module('degreeEvaluationsApp.degreeStructureServices', [])
  .factory('degreeStructureService', ['$resource', function($resource){
     var UserPlanResource = $resource('/degree-api/:id');
     var DegreeStructureResource = $resource('/degree-api/:userId/:id');
     var NodeSelectionResource = $resource('/degree-api/:id/course-selection/:componentId', null, { 'put': { 'method': 'PUT' } });
     var DegreeStructureService = {
        data: {
            userPlans: null,
            currentDegreeStructure: null
        },
        fetchUserPlans: function (userId){
            DegreeStructureService.data.userPlans = UserPlanResource.get({'id': userId});
        },
        getCurrentDegreeStructure: function(userId, id){
           DegreeStructureService.data.currentDegreeStructure = 
                DegreeStructureResource.get({'userId' : userId, 'id': id}, function process(data) {
                    if (!data.children) return;
                    for (var i=0; i<data.children.length; i++) {
                        data.children[i].parent = data;
                        process(data.children[i]);
                    }
                });
        },
        setSelected: function setSelected(id, node) {
            NodeSelectionResource.put({'id': id, 'componentId': node.id}, {'value': node.selected});
            if (node.selected && node.parent && !node.parent.selected) {
                node.parent.selected = true;
                setSelected(id, node.parent);
            }
        },
        clear: function(){
            DegreeStructureService.data.currentDegreeStructure = null;
            DegreeStructureService.data.userPlans = null;
        }
     };
     return DegreeStructureService;
  }]);

