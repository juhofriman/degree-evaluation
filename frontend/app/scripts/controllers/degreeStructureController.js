
'use strict';

angular.module('degreeEvaluationsApp')
  .controller('DegreeStructureController', ['$scope', 'degreeStructureService', 'studyRecordService', function ($scope, degreeStructureService, studyRecordService) {
    
    $scope.degreeStructureData = degreeStructureService.data;
    $scope.data = studyRecordService.data
    $scope.selectedStudent = null;
    
    /*
    $scope.degreeStructureData.userPlans.$promise.then(function(plans){
         degreeStructureService.getCurrentDegreeStructure(1, plans.degreeStructures[0].id);
    });*/
   

    $scope.editMode = false;
    $scope.selectNode = function(node){
        degreeStructureService.setSelected($scope.selectedStudent.id, node);
    }
    $scope.$on('selectionChanged', function(event, node) {
        degreeStructureService.setSelected($scope.selectedStudent.id, node);
    });
    
    $scope.selectPlan = function(plan){
        degreeStructureService.getCurrentDegreeStructure($scope.selectedStudent.id, plan.id);
    }
    
    $scope.selectStudent = function(student){
       if($scope.selectedStudent){
            if($scope.selectedStudent.id == student.id){
                $scope.selectedStudent = null;
            } else {
                 $scope.selectedStudent = student;
            }
       } else {
            $scope.selectedStudent = student;
            degreeStructureService.clear();
       }
        if($scope.selectedStudent != null){
            degreeStructureService.fetchUserPlans($scope.selectedStudent.id);
        } else {
            degreeStructureService.clear();
        }
    }

  }]);
  
  
 
