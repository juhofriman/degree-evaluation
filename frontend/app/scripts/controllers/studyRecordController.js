
'use strict';

angular.module('degreeEvaluationsApp')
  .controller('StudyRecordConroller', ['$scope', 'studyRecordService', function ($scope, studyRecordService) {
    
    console.log("studyRecordController");
    $scope.data = studyRecordService.data
    $scope.selectedStudent = null;
    $scope.evaluationDummys = [];
    
    studyRecordService.data.allIdeas.$promise.then(function(ideas){
        _.each(ideas, function(idea){
            var evaluationDummy = {};
            evaluationDummy.idIdea = idea.id;
            evaluationDummy.name = idea.name;
            evaluationDummy.grade = 0;
            $scope.evaluationDummys.push(evaluationDummy);
        });
    });
    $scope.studentHas = function(idIdea){
        return studyRecordService.currentStudyRecordsContain(idIdea);
    }

    $scope.selectStudent = function(student){
       if($scope.selectedStudent){
            if($scope.selectedStudent.id == student.id){
                $scope.selectedStudent = null;
            } else {
                 $scope.selectedStudent = student;
            }
       } else {
            $scope.selectedStudent = student;
             studyRecordService.data.studyRecords = null;
       }
        if($scope.selectedStudent != null){
             studyRecordService.getStudyRecords(student.id);
        } else {
             studyRecordService.data.studyRecords = null;
        }
    }
    $scope.saveEvaluation = function(evaluationDummy){
        studyRecordService.save($scope.selectedStudent.id, evaluationDummy);
    }
  }]);
  
  
 
