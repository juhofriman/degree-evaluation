package fi.uta.playlistproto.playlist;

import java.util.List;

/**
 * Created by Miika on 15.5.2014.
 */
public class UserPlaylist {
    String playlistName;
    String ownerName;
    List<UserPlaylistItem> selections;
}
