package fi.uta.playlistproto.playlist;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Miika on 15.5.2014.
 */
public class UserPlaylistDAO {
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDatasource(DataSource datasource){
        this.jdbcTemplate = new JdbcTemplate(datasource);
    }

    public Collection<UserPlaylist> findByUserId(Integer userId) {
        String sql = "SELECT id,name FROM playlist";

        Collection<UserPlaylist> pls = jdbcTemplate.query(sql, new ResultSetExtractor<Collection<UserPlaylist>>() {
            @Override
            public Collection<UserPlaylist> extractData(ResultSet rs) throws SQLException, DataAccessException {
                Map<Long, UserPlaylist> map = new TreeMap<>();
                while (rs.next()) {
                    Long id = rs.getLong("id");
                    UserPlaylist pl = map.get(id);
                    if (pl == null) {
                        pl = new UserPlaylist();
                        pl.playlistName = rs.getString("plname");
                        pl.ownerName = rs.getString("ownername");
                        map.put(id, pl);
                    }
                    else {
                        UserPlaylistItem item = new UserPlaylistItem();
                        item.name = rs.getString("item_name");
                        item.selected = rs.getBoolean("selected");
                        pl.selections.add(item);
                    }
                }
                return map.values();
            }
        });

        return pls;
    }

}
