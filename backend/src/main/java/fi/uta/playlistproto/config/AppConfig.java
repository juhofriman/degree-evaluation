package fi.uta.playlistproto.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.sql.DataSource;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages="fi.uta.playlistproto")
@PropertySource(value="classpath:jdbc.properties")
public class AppConfig extends WebMvcConfigurerAdapter {
    @Autowired
    Environment environment;

	@Override
	public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
		configurer.defaultContentType(MediaType.APPLICATION_JSON);
	}

    @Bean
    public DataSource getDatasource(){
        String driver = environment.getProperty("jdbc.driverclassname");
        String jdbcUrl = environment.getProperty("jdbc.connectionurl");
        String user = environment.getProperty("jdbc.username");
        String pass = environment.getProperty("jdbc.password");
        DriverManagerDataSource ds = new DriverManagerDataSource();
        ds.setDriverClassName(driver);
        ds.setUrl(jdbcUrl);
        ds.setUsername(user);
        ds.setPassword(pass);
        return ds;
    }

    @Bean
    public DataSourceTransactionManager getTxManager() {
        return new DataSourceTransactionManager(this.getDatasource());
    }


}
