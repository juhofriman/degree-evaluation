package fi.uta.playlistproto.degreestructure;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Miika on 16.5.2014.
 */
public class DegreeStructureNode {
    public Long id;
    public String name;
    public Integer ects;
    public boolean mandatory;
    public Boolean selected;
    public Boolean mount;
    public String grade;
    public String studyRecordName;
    public Date gradeDate;
    public List<DegreeStructureNode> children = new LinkedList<>();
}
