package fi.uta.playlistproto.degreestructure;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.JsonNode;

import fi.uta.playlistproto.degreestructure.UsersDegreeStructures.DegreeStructureReference;

/**
 * Created by Miika on 16.5.2014.
 */

/* TODO: move the userId parameter into the session by using spring security */

@Controller
@RequestMapping(value="/user-degree")
public class DegreeStructurePresenter {
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDatasource(DataSource datasource){
        this.jdbcTemplate = new JdbcTemplate(datasource);
    }
    
    @RequestMapping(value="/{userId}", method=RequestMethod.GET)
    @ResponseBody
    public UsersDegreeStructures getDegreeStructuresForUser(@PathVariable("userId") final Integer userId){
        String sql = "SELECT plan.*, texts.name "
                        + "FROM user_plan plan "
                        + "JOIN studycomponent_text texts on plan.id_studycomponent = texts.id_studycomponent "
                    + "WHERE plan.id_user = ? AND texts.lang LIKE 'fi'";
        return jdbcTemplate.query(sql, new Object[]{userId}, new ResultSetExtractor<UsersDegreeStructures>(){
            private UsersDegreeStructures usersDegreeStructures = new UsersDegreeStructures();
    
            @Override
            public UsersDegreeStructures extractData(ResultSet rs)
                    throws SQLException, DataAccessException {
                // KLUDGE
                if(!rs.next()){
                    usersDegreeStructures.userId = userId;
                    return usersDegreeStructures;
                }
                if(usersDegreeStructures.userId == null){
                    usersDegreeStructures.userId = rs.getInt("id_user");
                }
                do {
                    DegreeStructureReference degreeStructureReference = new UsersDegreeStructures.DegreeStructureReference();
                    degreeStructureReference.id = rs.getInt("id_studycomponent");
                    degreeStructureReference.name= rs.getString("name");
                    usersDegreeStructures.degreeStructures.add(degreeStructureReference);
                } while(rs.next());
                return usersDegreeStructures;
            }
        });

    }

    @RequestMapping(value="/{userId}/course-selection/{componentId}", method=RequestMethod.PUT)
    @ResponseBody
    public void setComponentSelected(@PathVariable("userId") Integer userId,
                                     @PathVariable("componentId") Integer componentId,
                                     @RequestBody JsonNode selectedObject) {
        boolean selected = selectedObject.get("value").asBoolean();
        if (selected) {
            jdbcTemplate.update(
                    "INSERT INTO user_selected_studycomponent (id_user, id_studycomponent) VALUES (?, ?) ",
                    userId,
                    componentId);
        }
        else {
            jdbcTemplate.update(
                    "DELETE FROM user_selected_studycomponent WHERE id_user = ? AND id_studycomponent = ?",
                    userId,
                    componentId
            );
        }
    }
    
    @RequestMapping(value="/{userId}/{degreeStructureId}", method=RequestMethod.GET)
    @ResponseBody
    public DegreeStructureNode get(@PathVariable("userId") Integer userId, @PathVariable("degreeStructureId") Integer degreeStructureId) {
        return jdbcTemplate.query("SELECT * FROM user_degreestructure WHERE id_user = ? AND rootc = ?", new ResultSetExtractor<DegreeStructureNode>() {
            @Override
            public DegreeStructureNode extractData(ResultSet rs) throws SQLException, DataAccessException {
                //parentpath -> [node] -- the list of children belonging to the *node* at *path*
                Map<String, List<DegreeStructureNode>> childrenByParentPath = new HashMap<>();
                Map<String, DegreeStructureNode> nodeByPath = new HashMap<>();
                DegreeStructureNode root = null;
                while (rs.next()) {
                    DegreeStructureNode node = new DegreeStructureNode();
                    node.id                 = rs.getLong    ("id");
                    node.name               = rs.getString  ("name");
                    node.ects               = rs.getInt     ("ects");
                    node.grade              = rs.getString  ("grade");
                    node.gradeDate          = rs.getDate    ("grade_date");
                    node.studyRecordName    = rs.getString  ("study_record_name");
                    node.selected           = rs.getBoolean ("selected");
                    node.mount              = rs.getBoolean ("is_user_mount");
                    node.mandatory          = rs.getBoolean ("mandatory");

                    String path         = rs.getString("path");
                    String parentPath   = rs.getString("parentpath");

                    if (parentPath == null) {
                        root = node;
                    }

                    /* populate maps for later use in assembling the tree */
                    nodeByPath.put(path, node);
                    if (!childrenByParentPath.containsKey(parentPath)) {
                        childrenByParentPath.put(parentPath, new LinkedList<DegreeStructureNode>());
                    }
                    childrenByParentPath.get(parentPath).add(node);
                }
                /* assemble the degree structure object tree:
                   for the node at *path*, assign node.children = childrenByParentPath[path]
                 */
                for (Map.Entry<String,List<DegreeStructureNode>> e : childrenByParentPath.entrySet()) {
                    String parentPath = e.getKey();
                    if (parentPath != null) {
                        nodeByPath.get(parentPath).children = e.getValue();
                    }
                }
                return root;
            }
        }, userId, degreeStructureId);
    }
}
