package fi.uta.playlistproto.degreestructure;

import java.util.LinkedList;
import java.util.List;

public class UsersDegreeStructures {

    public Integer userId;
    public List<DegreeStructureReference> degreeStructures = new LinkedList<>();
    
    public static class DegreeStructureReference {
        public String name;
        public Integer id;
    }
}
