package fi.uta.playlistproto.studyrecord;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value="/idea")
public class IdeaPresenter {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDatasource(DataSource datasource){
        this.jdbcTemplate = new JdbcTemplate(datasource);
    }

    @RequestMapping(value="", method=RequestMethod.GET)
    @ResponseBody
    public List<Idea> getIdeas(){
        return jdbcTemplate.query("SELECT * FROM idea", new RowMapper<Idea>(){
            @Override
            public Idea mapRow(ResultSet rs, int rowNum) throws SQLException {
               Idea idea = new Idea();
               idea.id = rs.getInt("id");
               idea.name = rs.getString("name");
               return idea;
            }
        });
    }
    
}
