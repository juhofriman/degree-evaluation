package fi.uta.playlistproto.studyrecord;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.JsonNode;

@Controller
@RequestMapping(value="/student")
public class StudentPresenter {
    
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDatasource(DataSource datasource){
        this.jdbcTemplate = new JdbcTemplate(datasource);
    }

    @RequestMapping(value="/{id}", method=RequestMethod.POST)
    @ResponseBody
    public void saveStudyRecord(@PathVariable("id") Integer studentId, @RequestBody JsonNode data){
        jdbcTemplate.update("INSERT INTO user_credits VALUES(default, ?, now(), ?, ?)", 
                new Object[]{data.path("grade").asInt(), studentId, data.path("idIdea").asInt()});
    }
    
    @RequestMapping(value="", method=RequestMethod.GET)
    @ResponseBody
    public List<Student> getStudents(){
        return jdbcTemplate.query("SELECT * FROM user_identity", new RowMapper<Student>(){
            @Override
            public Student mapRow(ResultSet rs, int rowNum) throws SQLException {
                Student student = new Student();
                student.id = rs.getInt("id");
                student.name = rs.getString("name");
                return student;
            }
        });
    }
    
    @RequestMapping(value="{id}", method=RequestMethod.GET)
    @ResponseBody
    public List<StudyRecord> getTranscript(@PathVariable("id") Integer studentId){
        return jdbcTemplate.query("SELECT * FROM user_credits cred JOIN idea ON cred.id_idea = idea.id WHERE id_user = ?",
                new Object[]{studentId},
                new RowMapper<StudyRecord>(){

                    @Override
                    public StudyRecord mapRow(ResultSet rs, int rowNum)
                            throws SQLException {
                        StudyRecord studyRecord = new StudyRecord();
                        studyRecord.id = rs.getInt("id");
                        studyRecord.grade = rs.getString("grade");
                        studyRecord.name = rs.getString("code") + " " + rs.getString("name");
                        studyRecord.gradeDate = rs.getDate("grade_date");
                        studyRecord.idIdea = rs.getInt("id_idea");
                        return studyRecord;
                    }});
    }

}
