package fi.uta.playlistproto.studyrecord;

import java.util.Date;

public class StudyRecord {

    public Integer id;
    public String name;
    public String grade;
    public Date gradeDate;
    public Integer idIdea;
}
