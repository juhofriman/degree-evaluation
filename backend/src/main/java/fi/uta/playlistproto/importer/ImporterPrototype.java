package fi.uta.playlistproto.importer;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Created by Miika on 16.5.2014.
 */
public class ImporterPrototype {

    private final Integer DEGREEPROGRAMME_TYPE = 0;
    private final Integer COURSEUNIT_TYPE = 1;
    private final Integer STUDYMODULE_TYPE = 2;

    private JdbcTemplate jt;

    public ImporterPrototype() {
        // create data source
        String driver = "org.postgresql.Driver";
        String jdbcUrl = "jdbc:postgresql:degree";
        String user = "degree";
        String pass = "degree";
        DriverManagerDataSource ds = new DriverManagerDataSource();
        ds.setDriverClassName(driver);
        ds.setUrl(jdbcUrl);
        ds.setUsername(user);
        ds.setPassword(pass);
        // create jdbctemplate
        this.jt = new JdbcTemplate(ds);
    }

    private void save(final JsonNode node) {
        try {
            jt.update("INSERT INTO idea VALUES(?, '', ?)", node.path("vkoodi").asInt(), node.path("texts").path("fi").path("name").asText());
            System.out.println("Viety hahmoäijä: " + node.path("vkoodi").asInt() + " " + node.path("texts").path("fi").path("name").asText());
        } catch (DataAccessException e1) {
            e1.printStackTrace();
            System.out.println("Oli jo");
        }
        KeyHolder k = new GeneratedKeyHolder();
        jt.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                /* default to courseunit, since they don't have the type field */
                Integer type = COURSEUNIT_TYPE;
                /* otherwise resolve based on the alternative strings */
                String typeString = node.path("type").asText();
                if (typeString.equals("STUDYMODULE")) {
                    type = STUDYMODULE_TYPE;
                } else if (typeString.equals("DEGREEPROGRAMME")) {
                    type = DEGREEPROGRAMME_TYPE;
                }
                String  extId       = externalId(node);
                Integer moduleECTS  = node.path("credits").asInt();
                Integer vkoodi      = node.path("vkoodi").asInt();
                String  criterion   = node.path("criterion").asText();
                if ("allOf".equals(criterion)) {
                    criterion = "all";
                }
                else {
                    criterion = "some";
                }
                PreparedStatement ps = connection.prepareStatement(
                        "INSERT INTO studycomponent (ects, externalid, sc_type, criterion, id_idea) VALUES (?, ?, ?, ?, ?)",
                        /* return auto-generated "id" field */
                        new String[] { "id" });
                ps.setInt   (1, moduleECTS);
                ps.setString(2, extId);
                ps.setInt   (3, type);
                ps.setString(4, criterion);
                ps.setInt   (5, vkoodi);
                return ps;
            }
        }, k);
        Long scId = k.getKey().longValue();
        /* do texts */
        JsonNode textsNode = node.path("texts");
        Iterator<String> langs = textsNode.fieldNames();
        while (langs.hasNext()) {
            String lang = langs.next();
            String name = textsNode.path(lang).path("name").asText();
            System.out.println(lang+":"+name);
            String outcomes = textsNode.path(lang).path("learningOutcomes").asText();
            String description = textsNode.path(lang).path("contentDescription").asText();
            jt.update(
                    "INSERT INTO studycomponent_text (id_studycomponent,    lang, outcomes,   description,    name) " +
                            " VALUES (?,                    ?,    ?,          ?,              ?) ",
                                      scId,                 lang, outcomes,   description,    name
            );
        }
        for (JsonNode child : node.path("learningOpportunities")) {
            try {
                save(child);
            } catch (Exception e) {
                System.out.println("already inserted this one...");
            }
        }
    }
    

    private Map<String, List<String>> findInclusions(JsonNode root) {
        String extId = externalId(root);
        Map<String, List<String>> map = new TreeMap<String,List<String>>();
        List<String> childExtIds = new LinkedList<String>();
        for (JsonNode child : root.path("learningOpportunities")) {
            childExtIds.add(externalId(child));
            map.putAll(findInclusions(child));
        }
        map.put(extId, childExtIds);
        return map;
    }

    private String externalId(JsonNode node) {
        String ns = node.path("externalId").path("ns").asText();
        String id = node.path("externalId").path("id").asText();
        String extId = ns + ":" + id;
        return extId;
    }

    public void run(String fn) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readValue(new File(fn), JsonNode.class);
        // save node and all its child nodes
        save(root);
        // get the child-parent connections between the nodes
        Map<String,List<String>> childExtIdsByExtId = findInclusions(root);
        System.out.println(childExtIdsByExtId);
        // save the connections also
        for (String parent : childExtIdsByExtId.keySet()) {
            for (String child : childExtIdsByExtId.get(parent)) {
                try {
                    jt.update(
                            "INSERT INTO studycomponent_inclusion (parent, child)" +
                                    " SELECT p.id, c.id FROM studycomponent p JOIN studycomponent C ON c.externalid = ? WHERE p.externalid = ?",
                            child, parent
                    );
                    System.out.println(parent + " is-parent-of " + child);
                } catch (DataAccessException e) {
                    System.out.println(parent + " already joined to " + child);
                }
            }
        }
    }

    public static void main(String[] args) throws IOException {
        ImporterPrototype i = new ImporterPrototype();
        i.run("dp.json");
        i.run("another-dp.json");
    }
}
