--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: idea; Type: TABLE; Schema: public; Owner: degree; Tablespace: 
--

CREATE TABLE idea (
    id integer NOT NULL,
    code text NOT NULL,
    name text NOT NULL
);


ALTER TABLE public.idea OWNER TO degree;

--
-- Name: idea_id_seq; Type: SEQUENCE; Schema: public; Owner: degree
--

CREATE SEQUENCE idea_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.idea_id_seq OWNER TO degree;

--
-- Name: idea_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: degree
--

ALTER SEQUENCE idea_id_seq OWNED BY idea.id;


--
-- Name: studycomponent; Type: TABLE; Schema: public; Owner: degree; Tablespace: 
--

CREATE TABLE studycomponent (
    id integer NOT NULL,
    ects integer NOT NULL,
    externalid text NOT NULL,
    sc_type integer NOT NULL,
    criterion text DEFAULT 'some'::text NOT NULL,
    id_idea integer
);


ALTER TABLE public.studycomponent OWNER TO degree;

--
-- Name: studycomponent_id_seq; Type: SEQUENCE; Schema: public; Owner: degree
--

CREATE SEQUENCE studycomponent_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.studycomponent_id_seq OWNER TO degree;

--
-- Name: studycomponent_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: degree
--

ALTER SEQUENCE studycomponent_id_seq OWNED BY studycomponent.id;


--
-- Name: studycomponent_inclusion; Type: TABLE; Schema: public; Owner: degree; Tablespace: 
--

CREATE TABLE studycomponent_inclusion (
    child integer NOT NULL,
    parent integer NOT NULL,
    id integer NOT NULL
);


ALTER TABLE public.studycomponent_inclusion OWNER TO degree;

--
-- Name: studycomponent_inclusion_id_seq; Type: SEQUENCE; Schema: public; Owner: degree
--

CREATE SEQUENCE studycomponent_inclusion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.studycomponent_inclusion_id_seq OWNER TO degree;

--
-- Name: studycomponent_inclusion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: degree
--

ALTER SEQUENCE studycomponent_inclusion_id_seq OWNED BY studycomponent_inclusion.id;


--
-- Name: studycomponent_text; Type: TABLE; Schema: public; Owner: degree; Tablespace: 
--

CREATE TABLE studycomponent_text (
    id_studycomponent integer NOT NULL,
    lang character(2) NOT NULL,
    outcomes text NOT NULL,
    description text NOT NULL,
    name text NOT NULL
);


ALTER TABLE public.studycomponent_text OWNER TO degree;

--
-- Name: studycomponent_type; Type: TABLE; Schema: public; Owner: degree; Tablespace: 
--

CREATE TABLE studycomponent_type (
    id integer NOT NULL,
    sc_type text NOT NULL
);


ALTER TABLE public.studycomponent_type OWNER TO degree;

--
-- Name: studycomponent_type_id_seq; Type: SEQUENCE; Schema: public; Owner: degree
--

CREATE SEQUENCE studycomponent_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.studycomponent_type_id_seq OWNER TO degree;

--
-- Name: studycomponent_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: degree
--

ALTER SEQUENCE studycomponent_type_id_seq OWNED BY studycomponent_type.id;


--
-- Name: user_component_mount; Type: TABLE; Schema: public; Owner: degree; Tablespace: 
--

CREATE TABLE user_component_mount (
    id integer NOT NULL,
    id_user integer NOT NULL,
    id_component_where integer NOT NULL,
    id_component_what integer NOT NULL
);


ALTER TABLE public.user_component_mount OWNER TO degree;

--
-- Name: user_component_mount_id_seq; Type: SEQUENCE; Schema: public; Owner: degree
--

CREATE SEQUENCE user_component_mount_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_component_mount_id_seq OWNER TO degree;

--
-- Name: user_component_mount_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: degree
--

ALTER SEQUENCE user_component_mount_id_seq OWNED BY user_component_mount.id;


--
-- Name: user_credits; Type: TABLE; Schema: public; Owner: degree; Tablespace: 
--

CREATE TABLE user_credits (
    id integer NOT NULL,
    grade text NOT NULL,
    grade_date date NOT NULL,
    id_user integer NOT NULL,
    id_idea integer NOT NULL
);


ALTER TABLE public.user_credits OWNER TO degree;

--
-- Name: user_credits_id_seq; Type: SEQUENCE; Schema: public; Owner: degree
--

CREATE SEQUENCE user_credits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_credits_id_seq OWNER TO degree;

--
-- Name: user_credits_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: degree
--

ALTER SEQUENCE user_credits_id_seq OWNED BY user_credits.id;


--
-- Name: user_identity; Type: TABLE; Schema: public; Owner: degree; Tablespace: 
--

CREATE TABLE user_identity (
    id integer NOT NULL,
    name text NOT NULL
);


ALTER TABLE public.user_identity OWNER TO degree;

--
-- Name: user_selected_studycomponent; Type: TABLE; Schema: public; Owner: degree; Tablespace: 
--

CREATE TABLE user_selected_studycomponent (
    id integer NOT NULL,
    id_user integer NOT NULL,
    id_studycomponent integer NOT NULL
);


ALTER TABLE public.user_selected_studycomponent OWNER TO degree;

--
-- Name: user_degreestructure; Type: VIEW; Schema: public; Owner: degree
--

CREATE VIEW user_degreestructure AS
 WITH RECURSIVE user_structure(rootc, id_user, id, parentpath, path, criterion, mandatory, is_user_mount) AS (
                 SELECT sc.id,
                    u.id,
                    sc.id,
                    NULL::integer[] AS int4,
                    ARRAY[sc.id] AS "array",
                    sc.criterion,
                    false AS bool,
                    false AS bool
                   FROM studycomponent sc,
                    user_identity u
        UNION ALL
                 SELECT s_1.rootc,
                    s_1.id_user,
                    COALESCE(i.child, m.id_component_what) AS "coalesce",
                    s_1.path,
                    (s_1.path || COALESCE(i.child, m.id_component_what)),
                    ( SELECT studycomponent.criterion
                           FROM studycomponent
                          WHERE ((studycomponent.id = i.child) OR (studycomponent.id = m.id_component_what))) AS criterion,
                        CASE s_1.criterion
                            WHEN 'all'::text THEN true
                            ELSE false
                        END AS "case",
                    (m.id_component_what IS NOT NULL)
                   FROM ((user_structure s_1
              LEFT JOIN studycomponent_inclusion i ON ((i.parent = s_1.id)))
         LEFT JOIN user_component_mount m ON (((m.id_component_where = s_1.id) AND (m.id_user = s_1.id_user))))
        WHERE (((m.id_component_where IS NOT NULL) AND (m.id_user = s_1.id_user)) OR (i.parent IS NOT NULL))
        )
 SELECT s.rootc,
    s.id_user,
    s.id,
    (s.parentpath)::text AS parentpath,
    (s.path)::text AS path,
    t.name,
    c.ects,
    (((((sel.id IS NOT NULL) OR s.mandatory) OR s.is_user_mount) OR (s.rootc = s.id)) OR (cred.grade IS NOT NULL)) AS selected,
    s.is_user_mount,
    s.mandatory,
    s.criterion,
    cred.grade,
    cred.grade_date,
    ((idea.code || ' '::text) || idea.name) AS study_record_name
   FROM (((((user_structure s
   JOIN studycomponent c ON ((c.id = s.id)))
   JOIN studycomponent_text t ON (((t.id_studycomponent = c.id) AND (t.lang = 'fi'::bpchar))))
   LEFT JOIN user_selected_studycomponent sel ON (((sel.id_studycomponent = c.id) AND (sel.id_user = s.id_user))))
   LEFT JOIN user_credits cred ON (((cred.id_idea = c.id_idea) AND (cred.id_user = s.id_user))))
   LEFT JOIN idea ON ((cred.id_idea = idea.id)))
  ORDER BY s.id_user, s.path;


ALTER TABLE public.user_degreestructure OWNER TO degree;

--
-- Name: user_plan; Type: TABLE; Schema: public; Owner: degree; Tablespace: 
--

CREATE TABLE user_plan (
    id integer NOT NULL,
    id_user integer NOT NULL,
    id_studycomponent integer NOT NULL
);


ALTER TABLE public.user_plan OWNER TO degree;

--
-- Name: user_plan_id_seq; Type: SEQUENCE; Schema: public; Owner: degree
--

CREATE SEQUENCE user_plan_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_plan_id_seq OWNER TO degree;

--
-- Name: user_plan_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: degree
--

ALTER SEQUENCE user_plan_id_seq OWNED BY user_plan.id;


--
-- Name: user_selected_studycomponent_id_seq; Type: SEQUENCE; Schema: public; Owner: degree
--

CREATE SEQUENCE user_selected_studycomponent_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_selected_studycomponent_id_seq OWNER TO degree;

--
-- Name: user_selected_studycomponent_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: degree
--

ALTER SEQUENCE user_selected_studycomponent_id_seq OWNED BY user_selected_studycomponent.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: degree
--

ALTER TABLE ONLY idea ALTER COLUMN id SET DEFAULT nextval('idea_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: degree
--

ALTER TABLE ONLY studycomponent ALTER COLUMN id SET DEFAULT nextval('studycomponent_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: degree
--

ALTER TABLE ONLY studycomponent_inclusion ALTER COLUMN id SET DEFAULT nextval('studycomponent_inclusion_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: degree
--

ALTER TABLE ONLY studycomponent_type ALTER COLUMN id SET DEFAULT nextval('studycomponent_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: degree
--

ALTER TABLE ONLY user_component_mount ALTER COLUMN id SET DEFAULT nextval('user_component_mount_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: degree
--

ALTER TABLE ONLY user_credits ALTER COLUMN id SET DEFAULT nextval('user_credits_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: degree
--

ALTER TABLE ONLY user_plan ALTER COLUMN id SET DEFAULT nextval('user_plan_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: degree
--

ALTER TABLE ONLY user_selected_studycomponent ALTER COLUMN id SET DEFAULT nextval('user_selected_studycomponent_id_seq'::regclass);


--
-- Data for Name: idea; Type: TABLE DATA; Schema: public; Owner: degree
--

COPY idea (id, code, name) FROM stdin;
0		Filosofian kandidaattiohjelma
39008		Tietotekniikkataidot
38279		Orientoivat opinnot ja HOPS
30131		Yhteiskunta- ja kulttuuritieteiden yhteiset opinnot
40845		Etiikka ja yhteiskuntafilosofia
40892		Suomalaisen yhteiskunnan historia vuodesta 1809 nykyaikaan
40780		Yhteiskunnan tutkimuksen ajankohtaisia kysymyksiä
40582		Toiminta, tulkinta ja tieto
40657		Yhteiskunnan mittaaminen
38750		Englannin kielen syventävä kurssi oikeustieteilijöille
38850		Tieteellisen seminaarin englanti
38825		Luova kirjoittaminen englanniksi
38780		Akateemiset esitelmät
38817		Työelämäviestintä
38830		Työelämän tekstit
38816		Väittelytaidon kurssi
38829		Tutkimusviestintä
38813		Kulttuurienvälinen viestintä
38824		Akateemista ja ammatillista englantia: itsenäinen oppimismoduuli
38823		Ammatillista englantia: Projektit ja kokoukset
38753		Juridisk svenska
38783		Min bransch i Norden
38826		Kohti gradua
38784		Tekstiklinikka
38852		Johdatus latinalaisen Amerikan kulttuuriin
38853		Johdatus espanjalaiseen kulttuuriin
38789		Espanjan kielen kirjallinen ja suullinen taito
38790		Espanjan kielioppia edistyneille
38851		Espanjan kielen syventävä kurssi
38841		Cours de Français approfondi - ranskan syventävä kurssi
38799		Ranska eilen, tänään ja huomenna
38800		Ranskan kielen suullinen harjoituskurssi
38806		Lesen, Verstehen, Schreiben
38845		Kommunikationstraining II
38805		Aktuelle Texte aus Presse und Studium
38844		Kommunikationstraining I
38846		Deutsch im Alltag und Studium
38812		Venäjän kielen suullinen harjoituskurssi
38811		Venäjän kielen luetunymmärtämis- ja keskustelukurssi
40338		Puheviestinnän perusteet
38762		Johdatus akateemiseen englantiin
38764		Tieteellinen kirjoittaminen
38763		Ruotsin kielen kirjallinen ja suullinen viestintä
40843		Tieto-oppi ja ontologia
40842		Johdatus filosofiaan ja sen historiaan
40844		Argumentaatio ja tieteenfilosofia
40846		Logiikka
40856		Kielifilosofia
40851		Yhteiskuntafilosofia
40853		Epistemologia
40850		Etiikka
40852		Kulttuuri- ja taiteenfilosofia
40854		Metafysiikka ja ontologia
40855		Mielen filosofia
40861		Kandidaattiseminaari ja kandidaatintutkielma
40849		Tieteenfilosofia ja tutkimusetiikka
40847		Filosofian historia
40864		Klassikot
40857		Proseminaari
40863		1900-luvun filosofia
40848		Filosofinen logiikka
98279		Johdatus louhintaan opiskelussa
\.


--
-- Name: idea_id_seq; Type: SEQUENCE SET; Schema: public; Owner: degree
--

SELECT pg_catalog.setval('idea_id_seq', 1, false);


--
-- Data for Name: studycomponent; Type: TABLE DATA; Schema: public; Owner: degree
--

COPY studycomponent (id, ects, externalid, sc_type, criterion, id_idea) FROM stdin;
2	5	tay:7842	2	all	0
3	3	tay:21013	1	some	39008
4	2	tay:22356	1	some	38279
5	0	tay:6771	2	some	30131
6	5	tay:20520	1	some	40845
7	5	tay:21471	1	some	40892
8	5	tay:22700	1	some	40780
9	5	tay:21304	1	some	40582
10	5	tay:21810	1	some	40657
11	0	tay:6165	2	some	0
12	0	tay:7556	2	some	0
13	0	tay:6659	2	all	0
14	2	tay:21043	1	some	38750
15	2	tay:22645	1	some	38850
16	2	tay:21444	1	some	38825
17	2	tay:21220	1	some	38780
18	2	tay:21389	1	some	38817
19	2	tay:21566	1	some	38830
20	2	tay:21385	1	some	38816
21	2	tay:21563	1	some	38829
22	2	tay:26184	1	some	0
23	2	tay:26183	1	some	0
24	2	tay:21324	1	some	38813
25	2	tay:21443	1	some	38824
26	2	tay:21442	1	some	38823
27	0	tay:6660	2	all	0
28	2	tay:21046	1	some	38753
29	2	tay:21226	1	some	38783
30	0	tay:6662	2	all	0
31	2	tay:21476	1	some	38826
32	2	tay:21227	1	some	38784
33	0	tay:7557	2	all	0
34	2	tay:22707	1	some	38852
35	2	tay:22708	1	some	38853
36	4	tay:21244	1	some	38789
37	2	tay:21245	1	some	38790
38	2	tay:22706	1	some	38851
39	0	tay:7558	2	all	0
40	4	tay:22074	1	some	38841
41	4	tay:21257	1	some	38799
42	2	tay:21258	1	some	38800
43	0	tay:7559	2	all	0
44	2	tay:21289	1	some	38806
45	2	tay:22299	1	some	38845
46	4	tay:21288	1	some	38805
47	2	tay:22208	1	some	38844
48	4	tay:22301	1	some	38846
49	0	tay:7560	2	all	0
50	2	tay:21295	1	some	38812
51	4	tay:21294	1	some	38811
52	2	tay:21149	1	some	40338
53	4	tay:21099	1	some	38762
54	3	tay:21101	1	some	38764
55	4	tay:21100	1	some	38763
56	25	tay:6113	2	all	0
58	5	tay:20516	1	some	40843
59	5	tay:20515	1	some	40842
60	5	tay:20518	1	some	40844
61	5	tay:20521	1	some	40846
62	50	tay:6114	2	all	0
63	10	tay:6115	2	some	0
64	5	tay:20532	1	some	40856
65	5	tay:20527	1	some	40851
66	5	tay:20529	1	some	40853
67	5	tay:20526	1	some	40850
68	5	tay:20528	1	some	40852
69	5	tay:20530	1	some	40854
70	5	tay:20531	1	some	40855
71	10	tay:20576	1	some	40861
72	5	tay:20525	1	some	40849
73	5	tay:20522	1	some	40847
74	5	tay:20744	1	some	40864
75	5	tay:20533	1	some	40857
76	5	tay:20743	1	some	40863
77	5	tay:20524	1	some	40848
78	65	tay:7461	2	some	0
80	5	tay:17842	2	all	0
81	3	tay:31013	1	some	39008
82	2	tay:32356	1	some	98279
83	0	tay:16771	2	some	30131
84	5	tay:30520	1	some	40845
85	5	tay:31471	1	some	40892
86	5	tay:32700	1	some	40780
87	5	tay:31304	1	some	40582
88	5	tay:31810	1	some	40657
89	0	tay:16165	2	some	0
90	0	tay:17556	2	some	0
91	0	tay:16659	2	all	0
92	2	tay:31043	1	some	38750
93	2	tay:32645	1	some	38850
94	2	tay:31444	1	some	38825
95	2	tay:31220	1	some	38780
96	2	tay:31389	1	some	38817
97	2	tay:31566	1	some	38830
98	2	tay:31385	1	some	38816
99	2	tay:31563	1	some	38829
100	2	tay:36184	1	some	0
101	2	tay:36183	1	some	0
102	2	tay:31324	1	some	38813
103	2	tay:31443	1	some	38824
104	2	tay:31442	1	some	38823
105	0	tay:16660	2	all	0
106	2	tay:31046	1	some	38753
107	2	tay:31226	1	some	38783
108	0	tay:16662	2	all	0
109	2	tay:31476	1	some	38826
110	2	tay:31227	1	some	38784
111	0	tay:17557	2	all	0
112	2	tay:32707	1	some	38852
113	2	tay:32708	1	some	38853
114	4	tay:31244	1	some	38789
115	2	tay:31245	1	some	38790
116	2	tay:32706	1	some	38851
117	0	tay:17558	2	all	0
118	4	tay:32074	1	some	38841
119	4	tay:31257	1	some	38799
120	2	tay:31258	1	some	38800
121	0	tay:17559	2	all	0
122	2	tay:31289	1	some	38806
123	2	tay:32299	1	some	38845
124	4	tay:31288	1	some	38805
125	2	tay:32208	1	some	38844
126	4	tay:32301	1	some	38846
127	0	tay:17560	2	all	0
128	2	tay:31295	1	some	38812
129	4	tay:31294	1	some	38811
130	2	tay:31149	1	some	40338
131	4	tay:31099	1	some	38762
132	3	tay:31101	1	some	38764
133	4	tay:31100	1	some	38763
134	25	tay:16113	2	all	0
136	5	tay:30516	1	some	40843
137	5	tay:30515	1	some	40842
138	5	tay:30518	1	some	40844
139	5	tay:30521	1	some	40846
140	50	tay:16114	2	all	0
141	10	tay:16115	2	some	0
142	5	tay:30532	1	some	40856
143	5	tay:30527	1	some	40851
144	5	tay:30529	1	some	40853
145	5	tay:30526	1	some	40850
146	5	tay:30528	1	some	40852
147	5	tay:30530	1	some	40854
148	5	tay:30531	1	some	40855
149	10	tay:30576	1	some	40861
150	5	tay:30525	1	some	40849
151	5	tay:30522	1	some	40847
152	5	tay:30744	1	some	40864
153	5	tay:30533	1	some	40857
154	5	tay:30743	1	some	40863
155	5	tay:30524	1	some	40848
156	65	tay:17461	2	some	0
1	180	tay:1330	0	all	0
79	180	tay:21330	0	all	0
\.


--
-- Name: studycomponent_id_seq; Type: SEQUENCE SET; Schema: public; Owner: degree
--

SELECT pg_catalog.setval('studycomponent_id_seq', 156, true);


--
-- Data for Name: studycomponent_inclusion; Type: TABLE DATA; Schema: public; Owner: degree
--

COPY studycomponent_inclusion (child, parent, id) FROM stdin;
2	1	1
5	1	2
11	1	3
56	1	4
62	1	5
78	1	6
6	56	7
58	56	8
59	56	9
60	56	10
61	56	11
63	62	12
71	62	13
72	62	14
73	62	15
74	62	16
75	62	17
76	62	18
77	62	19
64	63	20
65	63	21
66	63	22
67	63	23
68	63	24
69	63	25
70	63	26
12	11	27
52	11	28
53	11	29
54	11	30
55	11	31
14	13	32
15	13	33
16	13	34
17	13	35
18	13	36
19	13	37
20	13	38
21	13	39
22	13	40
23	13	41
24	13	42
25	13	43
26	13	44
28	27	45
29	27	46
31	30	47
32	30	48
6	5	49
7	5	50
8	5	51
9	5	52
10	5	53
13	12	54
27	12	55
30	12	56
33	12	57
39	12	58
43	12	59
49	12	60
34	33	61
35	33	62
36	33	63
37	33	64
38	33	65
40	39	66
41	39	67
42	39	68
44	43	69
45	43	70
46	43	71
47	43	72
48	43	73
50	49	74
51	49	75
3	2	76
4	2	77
84	134	78
136	134	79
137	134	80
138	134	81
139	134	82
141	140	83
149	140	84
150	140	85
151	140	86
152	140	87
153	140	88
154	140	89
155	140	90
142	141	91
143	141	92
144	141	93
145	141	94
146	141	95
147	141	96
148	141	97
90	89	98
130	89	99
131	89	100
132	89	101
133	89	102
92	91	103
93	91	104
94	91	105
95	91	106
96	91	107
97	91	108
98	91	109
99	91	110
100	91	111
101	91	112
102	91	113
103	91	114
104	91	115
106	105	116
107	105	117
109	108	118
110	108	119
84	83	120
85	83	121
86	83	122
87	83	123
88	83	124
91	90	125
105	90	126
108	90	127
111	90	128
117	90	129
121	90	130
127	90	131
112	111	132
113	111	133
114	111	134
115	111	135
116	111	136
118	117	137
119	117	138
120	117	139
122	121	140
123	121	141
124	121	142
125	121	143
126	121	144
128	127	145
129	127	146
81	80	147
82	80	148
80	79	149
83	79	150
89	79	151
134	79	152
140	79	153
156	79	154
\.


--
-- Name: studycomponent_inclusion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: degree
--

SELECT pg_catalog.setval('studycomponent_inclusion_id_seq', 154, true);


--
-- Data for Name: studycomponent_text; Type: TABLE DATA; Schema: public; Owner: degree
--

COPY studycomponent_text (id_studycomponent, lang, outcomes, description, name) FROM stdin;
1	en	After completing the Bachelor's Programme in philosophy  students are expected to recognize the main subfields of philosophy, to know the philosophical problems pertaining to those fields and to be familiar with the historical background of those problems. They understand how philosophical problems both differ from and are connected to the problems of other disciplines. Students are familiar with the basic concepts, trends and representatives of the subfields of philosophy. They are trained in logical inference, and they recognize and are able to analyze arguments in various texts, as well as to put forth their own arguments. They are capable of writing a scientific philosophical presentation and of taking part in philosophical discussion. To some extent, they are also familiar with the main problems, concepts and contents of another discipline or other disciplines they have chosen to study. They have basic skills in information technology, and skills in languages and communication sufficient for their future tasks, and they are familiar with the main concepts and problems of social sciences and humanities.	Studies in philosophy that belong to the Bachelor's Programme provide the students with an overview of the nature and the history of philosophical thought. They give an introduction to the basic concepts and views of the main subfields of philosophy. It is recommended that the student chooses a well considered and sufficiently wide combination of studies in other disciplines; those studies both support studies in philosophy and provide the student with wide competence that helps in building a career after graduating from the Master's Programme. The Bachelor's Thesis and the proseminar provide the students with the study skills that are needed in Master studies. The Bachelor's Programme also contains studies in languages and communication, career skills and information technology, the joint studies of the School of Social Sciences and Humanities, and studies in another discipline or other disciplines on the student's own choice.	Bachelor's Programme in Philosophy
1	fi	Kandidaatin tutkinnon suoritettuaan opiskelija tunnistaa filosofian osa-alueet ja tuntee niille ominaiset filosofiset ongelmat sekä niiden historiallisen taustan. Hän ymmärtää, miten filosofiset ongelmat yhtäältä eroavat muiden tieteenalojen tutkimusongelmista ja toisaalta kytkeytyvät niihin.  Hän tuntee filosofian eri osa-alueiden keskeisiä käsitteitä, suuntauksia ja niiden edustajia. Opiskelija on harjaantunut loogisen päättelyn taidoissa, tunnistaa ja osaa analysoida argumentteja erilaisissa kirjoituksissa sekä esittää omia argumenttejaan. Hän osaa kirjoittaa käsittelytavaltaan filosofisen esityksen ja osallistua filosofiseen keskusteluun. Hän on valintansa mukaan perehtynyt myös jonkin verran toisen tieteenalan tai toisten tieteenalojen keskeisiin kysymyksiin, käsitteisiin ja sisältöihin. Hän hallitsee tietotekniset taidot, hänellä on opintojensa ja tulevien tehtäviensä kannalta riittävät kieli- ja viestintätaidot, ja hän tunnistaa  eri yhteiskunta- ja kulttuuritieteiden peruskäsitteitä ja keskeisiä ongelmia.	Kandidaatin tutkintoon sisältyvät filosofian opinnot antavat opiskelijalle yleiskuvan filosofisen ajattelun, sekä sen teoreettisten että käytännöllisten pääalueiden, luonteesta ja historiallisesta kehityksestä. Ne tutustuttavat filosofian keskeisimpien osa-alueiden peruskäsitteisiin ja näkemyksiin. Opiskelijan on suotavaa valita mielekäs ja riittävän laaja sivuaineopintojen kokonaisuus, joka yhtäältä tukee opiskelijan filosofista suuntautumista ja toisaalta antaa sellaista laaja-alaista osaamista, joka auttaa sijoittumista työelämään maisteriopintojen jälkeen. Kandidaattitutkielman ja proseminaarin tavoitteena on perehdyttää opiskelija maisteriopinnoissa vaadittaviin opiskelutaitoihin. Tutkintoon sisältyvät kieli- ja viestintäopinnot, urataitojen ja tietotekniikan opiskelua, yhteiskunta- ja kulttuuritieteiden yksikön yhteiset opinnot sekä filosofian opiskelijan oman valinnan mukaan toisen tieteenalan tai toisten tieteenalojen opintoja.	Filosofian kandidaattiohjelma
2	en	null	null	Study skills
2	fi			Opiskeluvalmiudet
3	en			Introduction to Computing
3	fi	Opintojakson suoritettuaan\r\n- opiskelija tuntee yliopiston tietoteknisen ympäristön, osaa toimia mikroluokissa ja osaa käyttää yliopiston tarjoamia tietoteknisiä palveluita\r\n- opiskelijan osaa käyttää yleisimpiä sovellusohjelmistoja tehokkaasti ja monipuolisesti ja perussovellusten käyttötaidot ovat ajantasalla\r\n- opiskelijalla on valmiudet ylläpitää tietoteknisiä valmiuksiaan omatoimisesti		Tietotekniikkataidot
4	en	The module aims at giving the students basic information on stdying in universities in general and in the University of Tampere in particular, and introduce them to planning their studies.\r\n\r\nDuring their first year of study, students write a personal study plan, which confirms the structure of the degree and combination of subjects the student will study. The student will be given guidance in writing the personal study plan by the major subject department and the ISSS.		Orientation and Personal Study Plan
4	fi	Opintojakson tarkoituksena on antaa opiskelijoille perustiedot yliopisto-opiskelusta yleensä ja erityisesti opiskelusta Tampereen yliopistossa, johdattaa oman tutkinto-ohjelman koulutustavoitteisiin ja opintojen suunnitteluun.\r\nOpiskelija laatii ensimmäisen opiskeluvuotensa aikana henkilökohtaisen opintosuunnitelman (HOPS), jonka yhteydessä varmistetaan muun muassa tutkinnon rakenne ja aineyhdistelmä. Henkilökohtainen opintosuunnitelma on ennen kaikkea opiskelijan oma opintojen suunnittelun apuväline, jonka laatimiseen hän saa ohjausta oman oppiaineensa opettajilta  sekä yksikön opintoneuvonnasta.		Orientoivat opinnot ja HOPS
5	en	null	null	Joint Studies / YKY
5	fi	Yhteiskunta- ja kulttuuritieteiden yksikön kaikkien tutkinto-ohjelmien opiskelijoille yhteiset opinnot suoritettuaan opiskelija osaa tulkita yhteiskuntaa ja kulttuuria ja hänellä on käsitys tutkimuksen suhteesta yhteiskuntaan, sen eri toimijoihin ja instituutioihin. \r\n\r\nOpiskelija ymmärtää yhteiskunta- ja kulttuuritieteellisen tutkimuksen tapoja muotoilla tutkimuksen kysymyksiä, tuntee tutkimuksen peruskäsitteitä ja on alustavasti perehtynyt määrällisen ja laadullisen tutkimuksen menetelmiin. Hän tunnistaa, ymmärtää ja osaa arvioida eri tavoin yhteiskunnasta ja ihmisistä kerättyä ja tuotettua tietoa sekä tietää, missä ja miten sitä kerätään ja missä sitä on saatavilla. \r\n\r\nOpiskelija tuntee Suomen lähihistorian keskeiset ilmiöt ja osaa eritellä niitä osana laajempaa eurooppalaista yhteiskuntakehitystä.  Hän tuntee etiikan ja yhteiskuntafilosofian tärkeimmät historialliset muotoilut sekä yleisimmät modernissa etiikassa ja yhteiskuntafilosofiassa esitetyt kuvaukset yksilön, yhteisön ja yhteiskunnan suhteista sekä niitä jäsentävästä kulttuurin käsitteestä. \r\n\r\nOpintokokonaisuuden käytyään opiskelija osaa sijoittaa oman tutkinto-ohjelmansa erityisen  aineksen osaksi laajempaa yhteiskunta- ja kulttuuritutkimuksen tieteenalojen kenttää. Hän on myös tutustunut yliopisto-opiskelussa sovellettaviin opetuksen muotoihin, opetusteknologioihin ja suoritustapoihin. 	Tutkintoon vaadittavien yhteisten opintojen määrä riippuu oman tutkinto-ohjelman vaatimuksista. Kaikkien tutkinto-ohjelmien opiskelijat suorittavat pakollisena opintojakson Yhteiskunnan tutkimuksen ajankohtaisia kysymyksiä.\r\n\r\nHistorian ja psykologian tutkinto-ohjelman opiskelijat valitsevat lisäksi neljästä yhteisten opintojen opintojaksosta kolme, logopedian tutkinto-ohjelman opiskelijat vastaavasti kaksi opintojaksoa. Yksikön muiden tutkinto-ohjelmien opiskelijat suorittavat yhteiset opinnot kokonaisuudessaan.	Yhteiskunta- ja kulttuuritieteiden yhteiset opinnot
6	en	The participants are aware of the most important historical approaches to ethics and social philosophy and of the principal modern conceptions of the relation between individual and community. They know the ethical and moral questions about goodness and justice. They are familiar with the ontological question of what human beings are in a community, and with the epistemological question of what human communities are as objects and subjects of knowledge. They are capable of reflecting on the philosophical assumptions implicit in contemporary moral and political debates.		Ethics and Social Philosophy
37	fi	Kurssi on tarkotettu opiskelijoille, joilla on hyvä espanjan kielen taito, mutta jotka haluavat syventää kielioppitaitojaan.	.	Espanjan kielioppia edistyneille
112	en			Introduction to Latin America Culture
6	fi	Opintojakson suoritettuaan opiskelija tuntee etiikan ja yhteiskuntafilosofian tärkeimmät historialliset muotoilut sekä tärkeimmät modernissa etiikassa ja yhteiskuntafilosofiassa esitetyt kuvaukset yksilön, yhteisön ja yhteiskunnan suhteista sekä niitä jäsentävästä kulttuurin käsitteestä. Hän hahmottaa ennen kaikkea eettisen ja moraalisen kysymyksen siitä, mitä on hyvyys ja oikeudenmukaisuus yhteisössä. Hänen tuntee peruspiirteissään myös ontologisen kysymyksen siitä, mikä on ihminen yhteisössä, sekä epistemologisen kysymyksen ihmisyhteisöstä tiedon kohteena ja tuottajana. Hän osaa myös reflektoida eettisten, moraalisten ja yhteiskunnallisten kysymysten filosofisia taustaoletuksia.	Opintojakso vastaa kesään 2012 voimassa olleen opintosuunnitelman jaksoja seuraavasti:\r\n- YKYY5a Johdatus etiikkaan vastaa jaksoa PHILPE5 ja\r\n- YKYY5b Johdatus yhteiskuntafilosofiaan jaksoa PHILPE6.	Etiikka ja yhteiskuntafilosofia
7	en			History of Finnish Society from the 19th to 21th century
7	fi	Opintojakson suoritettuaan opiskelija tuntee ajan keskeiset ilmiöt ja osaa analysoida niitä osana laajempaa eurooppalaista yhteiskuntakehitystä. Opiskelija osaa kuvata aikakautta koskevien historiakäsitysten keskeisiä muutoksia.		Suomalaisen yhteiskunnan historia vuodesta 1809 nykyaikaan
8	en			Current Issues in Social Sciences
8	fi	Opiskelijalla on peruskäsitys yhteiskuntaa ja kulttuuria koskevan tutkimuksen suhteesta yhteiskuntaan, sen eri toimijoihin ja instituutioihin. Opiskelija ymmärtää esimerkkien avulla, miten eri ihmistieteiden alat jäsentävät nykymaailmaa, miten ne asettavat sitä koskevia kysymyksiä ja miten tutkimuksen tuottama tieto ja näkemykset syntyvät vastauksena näihin kysymyksiin.  Hän osaa alustavasti suhteuttaa ajankohtaista yhteiskuntakeskustelua tutkimustietoon, osaa erottaa tutkimuksen tiedonintressejä muiden toimijoiden näkökulmista ja oppii suhteuttamaan omaa ymmärrystään tutkimuksen tapaan käsitteellistää ajankohtaisia kysymyksiä. \r\n\r\nOpiskelija osaa etsiä opinnoissa tarvittavia kirjoja sekä tietyn aihepiirin painettuja ja elektronisia aineistoja (e-lehdet, e-kirjat) erilaisia tietokantapalveluita hyödyntäen. Opiskelija ymmärtää lähdekritiikin tärkeyden ja tietää tieteellisen viittauskäytännön periaatteet.		Yhteiskunnan tutkimuksen ajankohtaisia kysymyksiä
9	en			Action, Interpretation, and Knowledge
9	fi	Opiskelija hallitsee yhteiskunta- ja kulttuuritieteellisen tutkimuksen peruskäsitteitä, tuntee laadullisen tutkimuksen menetelmiä, osaa lukea ja arvioida laadullista tutkimusta sekä arvostaa erilaisia tiedon muotoja. Opiskelija ymmärtää ihmisten toiminnan ja kokemuksen tutkimuksen merkityksen. Hän osaa asettaa tiedon ja vallan väliseen suhteeseen liittyviä kysymyksiä ja keskustella niistä. Hän tunnistaa yhteiskunnallisten erojen ja hierarkioiden merkityksen yhteiskunnallisen tiedon tuotannossa ja ymmärtää yhteiskunnallisen marginalisaation, huono-osaisuuden ja epäoikeudenmukaisuuden kysymyksiä.		Toiminta, tulkinta ja tieto
10	en			Measuring society
10	fi	Kurssin tavoite on kehittää yhteiskuntaa koskevan määrällisen tiedon lukutaitoa. Opiskelija hallitsee perustasolla määrällisen tutkimuksen käsitteet sekä tuntee numeeristen aineistojen perustyypit ja rakentumisen tavat.  Hän osaa myös esittää tutkimustietoa koskevia kysymyksiä ja arvioida numerotiedon luotettavuutta. Opiskelija tunnistaa ja ymmärtää eri tavoin yhteiskunnasta ja ihmisistä kerättyä ja tuotettua tietoa sekä tietää, missä ja miten sitä kerätään ja missä sitä on saatavilla.		Yhteiskunnan mittaaminen
11	en			School of Social Sciences and Humanities, B.A.
11	fi	Tutkintoihin kuuluvat akateemiset kieli- ja viestintäopinnot tukevat\r\n\r\n* oman alan opiskelua, alan kehityksen seuraamista ja aineistojen hyödyntämistä\r\n* kieli- ja viestintätaidon kehittämistä\r\n* asiantuntijuuden kehittymistä\r\n* tieteellisten käytäntöjen oppimista\r\n* kotimaisissa ja kansainvälisissä yhteyksissä toimimista\r\n* yksilön monikielisyyttä ja monikulttuurisuutta	Humanististen tieteiden kandidaatin tutkintoon kuuluvat seuraavat kieli- ja viestintäopinnot (15 op):\r\n\r\n- vieraan kielen opintojakso 4 op (taitotasolla B1-B2)\r\n- ruotsin kielen kirjallinen ja suullinen viestintä 4 op\r\n- tieteellinen kirjoittaminen 3 op\r\n- puheviestinnän perusteet 2 op\r\n- valinnainen opintojakso 2 op\r\n\r\nTutkintonsa valinnaiseksi opintojaksoksi opiskelija voi valita suomen, ruotsin tai vieraan kielen opintojakson taitotasolta B1-C2. Valinnaiset opintojaksot on kuvattu opetussuunnitelmassa kyseisten kielten kohdalla. Valinnaisten opintojaksojen tarjonta vaihtelee lukuvuosittain.\r\n\r\nVieraan kielen opintojakso (4 op) on tarkoitettu suoritettavaksi 1. opintovuonna, ja ruotsin kielen kirjallisen ja suullisen viestinnän opintojakso (4 op) 2. opintovuonna. Suomen kielen tieteellisen kirjoittamisen opintojakso on tarkoitus suorittaa 3. opintovuonna.\r\n\r\nEdellä mainittujen kieli- ja viestintäopintojen (15 op) lisäksi opiskelija voi suorittaa myös muita kielikeskuksen tarjoamia kieli- ja viestintäopintoja ja sisällyttää ne tutkinnon vapaavalintaisiin opintoihin.\r\n	Yhteiskunta- ja kulttuuritieteiden yksikkö, HuK, kieli- ja viestintäopinnot
12	en	null	null	Optional Courses
12	fi		Useimpiin kandidaatin tutkintoihin kuuluu yhteensä kuusi opintopistettä vieraan kielen opintoja: yksi 4 opintopisteen opintojakso sekä yksi 2 opintopisteen valinnainen opintojakso. Opiskelija voi suorittaa tutkintoonsa myös kaksi 4 opintopisteen vieraan kielen opintojaksoa (esim. ranskaa 4 op ja saksaa 4 op). Tällöin molempien opintojaksojen on oltava vähintään taitotasoa B1, englannin opintojakson vähintään taitotasoa B2. Kieli- ja viestintäopintoja on tällöin yhteensä 17 opintopistettä.\r\n\r\nTutkintoihin soveltuvat vieraiden kielten sekä suomen ja ruotsin kielen valinnaiset opintojaksot on listattu alla. Opintojaksot löytyvät myös kyseisten kielten kohdalta.	Valinnaiset opintojaksot
13	en			Optional English Courses
13	fi		Useimpiin kandidaatin tutkintoihin voi sisältyä yhteensä kuusi opintopistettä vieraan kielen opintoja: yksi neljän opintopisteen opintojakso taitotasoa B1-B2 sekä yksi kahden opintopisteen opintojakso taitotasoa B1-C2. Opintojaksot ovat yleensä opiskelijan vapaasti valittavissa.\r\n\r\nSeuraavassa on lueteltu kandidaatin tutkintoon soveltuvat englannin kielen valinnaiset opintojaksot (2 op). Opintojaksojen tarjonta saattaa vaihdella lukuvuosittain. KTM-tutkintoa suorittavilla oikeustieteiden opiskelijoilla on etusija Legal English -kurssille.	Englannin kielen valinnaiset opintojaksot
14	en	On completion of the course students are expected to:\r\n\r\n- be able to understand demanding academic and professional texts relating to their field;\r\n- be able to discuss issues relating to their own field with both colleagues and laymen;\r\n- be able to give a presentation relating to their academic field making use of suitable technology; be able to deal with questions that may arise in the course of a presentation and to lead a discussion arising from a presentation;\r\n-be able to write texts typical for written communication in professional life;	Students will  widen and deepen their knowledge of legal English so that they are able to communicate effectively in working life.	Legal English
14	fi			Englannin kielen syventävä kurssi oikeustieteilijöille
15	en	On completion of the course students are expected to:\r\n\r\n-be aware of the typical conventions of academic seminars and texts;\r\n-be able to understand the differences between statements based on facts, opinions and feelings;\r\n-be able to participate actively in seminar in all the possible roles (defender, opponent, chair or group member);\r\n-be able to construct a justified argumentation chain by expressing thoughts logically and presenting relevant evidence;\r\n-be able to hold a seminar presentation;\r\n-be able to write a presentation abstract;	The purpose of the course is to raise the level of students skills in academic English so that they can participate actively in an international academic seminar.	Seminar Skills
15	fi			Tieteellisen seminaarin englanti
16	en	Students will \r\n\r\n-\tproduce a body of original writing in English.\r\n-\tproduce an analysis of their own writing in which they explain their objectives and how they achieved them.\r\n-\tlearn how to review creative writing critically and to discuss their critical evaluations clearly.\r\n-\tdevelop their self-critical and self-editing skills.\r\n\r\nIn addition, students will be encouraged to publish their work online and to organize and participate in a public reading of work produced on the course.	The aim of the course is for students to develop their powers of expression in written English via the medium of creative writing and to understand how they can produce various responses in the reader.	Creative Writing in English
16	fi			Luova kirjoittaminen englanniksi
17	en	On completion of the course, students are expected to be able to:\r\n\r\n\tprepare and deliver a discipline-specific, academic presentation that follows accepted norms and good academic practice\r\n\twrite an abstract\r\n\tfind and use resources to facilitate and support the above\r\n\tpresent information clearly and convincingly\r\n\tcommunicate with confidence within both academic and workplace contexts \r\n\twork effectively in groups, negotiating and contributing to the completion of the tasks	The course focuses on giving a presentation and writing an abstract.\r\nStudents will also improve their oral communication skills in academic and professional situations relating to their own field and future profession. Students will become familiar with the conventions and cultural considerations associated with spoken production and interaction. In addition, students will develop communication confidence and team working skills.	Advanced Academic Presentations
17	fi			Akateemiset esitelmät
18	en	After successful completion of the course the students will\r\n\r\n be able to understand and know how to overcome barriers to effective communication\r\n have knowledge and tools to reflect on and develop their own communication skills\r\n be able to use strategies to enhance dialogue and constructive communication at work	The course aims to provide students with such communication skills that help further dialogue in the work place when using English as a lingua franca.	Talk at Work: Dialogue
18	fi			Työelämäviestintä
19	en	Upon successful completion of the course, students will be able to\r\n\tunderstand and apply the basics of written communication in their work\r\n\twrite documents related to work life professional writing\r\n\tuse appropriate terminology of their professional field \r\n\tfollow the conventions of academic and professional written communication\r\n\tconsider cultural aspects in professional writing	The course aims to provide the student with English professional writing competencies needed in working life.	Professional Writing
19	fi			Työelämän tekstit
20	en	After successful completion of the course the students will be able to\r\n\r\n use sources to research debate topics and prepare speeches\r\n construct arguments and use appropriate language in argumentative speech\r\n recognize and use rhetorical devices\r\n use language typical of debates\r\n give informative and persuasive speeches in a well-structured manner\r\n evaluate, analyze and react to arguments critically \r\n function in groups according to the roles prescribed by the debating rules\r\n control timing when giving speeches		Debating for Academic Purposes
20	fi			Väittelytaidon kurssi
21	en	Upon successful completion of the course, students will be able to\r\n\tuse various cohesive devices to improve the flow of their text\r\n\tknow and employ a formal academic style (grammar and vocabulary)\r\n\texpress their own voice in their writing\r\n\tidentify the structure, moves and linguistic features of a research article\r\n\twrite a data commentary and/or a literature review\r\n\tconstruct a research paper on a topic in their area of study	The course aims to provide the student with the competencies to write a well-structured and stylistically appropriate research article in English with a view to eventual publication.	Writing for Research
21	fi			Tutkimusviestintä
22	en	On completion of the course, students are expected to be able to:\r\n- use some participatory methods successfully in English\r\n- have a good insight into the types of support necessary for successful collaboration and co-creation in English\r\n- design, plan and facilitate a short participatory workshop session in English\r\n- reflect on and assess the impact of your personal style in facilitation and communication in English\r\n- reflect on and assess your ability to collaborate in a multidisciplinary group in English	Today's fast-changing and complex working environments require us to engage in multiple styles of leading and learning. The ability to facilitate processes, collaborate and co-create in multidisciplinary /multiprofessional teams and use participatory methods to engage others are core academic / professional skills of university graduates. In this course, students will experience and experiment with a broad range of participatory methodologies and tools to facilitate group processes in English.	Facilitating, Engaging and Co-creating
22	fi			Fasilitointi, osallistavuus ja yhteiskehittely
23	en	On completion of the course, students are expected to be able to\r\n-\tdevelop a deeper understanding of the role of English in academic texts\r\n-\tlearn to identify the distinctiveness of academic English\r\n-\tapply the knowledge acquired in the course to  written assignments and whereby engage with the conventions of academic writing in their discipline	Academic discourse and practice has been informed by discourse analysis, narratology and numerous other approaches. This interdisciplinary course examines several of these approaches in various disciplines in order to get a fuller understanding of the role of discourse in academic texts and research.\r\nThe readings dealt with in the course will be reviewed and the contents of the articles discussed paying particular attention to the actual discourse of academic articles. Students will select a particular methodology/ approach/theory and apply it to their area of specialization.	Academic Discourse in Theory and Practice
23	fi			Akateeminen diskurssi teoriassa ja käytännössä
76	en	The student knows the principal philosophical currents of 20th century philosophy. S/he is also aware of their cultural and social context, of their background in modern philosophy, and of their interaction. The aim is also to understand the theoretical background of contemporary systematic questions.		Twentieth Century Philosophy
24	en	Upon successful completion of the course, students will\r\n\thave a better understanding of the influence of culture on language and communication\r\n\thave practiced and developed their intercultural communication skills is real situations with people from different cultural backgrounds\r\n\tbe able to evaluate and plan the development of their own intercultural communication skills\r\n\tbe more aware of cultural difference	The aim of the course is to become more aware of the influence of culture on language and communication, and to improve intercultural communication skills through discussion and interaction in mixed groups of Finnish and international students. The group work part of the course will be integrated with Cultural Conversations in the Intercultural Communication Studies program.	Intercultural Communication
24	fi			Kulttuurienvälinen viestintä
25	en	Negotiated with the participating students.	The aim of the module is to assist the participating students in identifying their own academic and/or professional learning outcomes (in groups and/or individually), to help them structure a course of independent study to meet those outcomes, to provide ongoing guidance and support, and finally to assess with them the results of their learning.	Academic/Professional English: Independent Learning Module
25	fi			Akateemista ja ammatillista englantia: itsenäinen oppimismoduuli
26	en	Upon successful completion of the course, students will be able to\r\n\tunderstand the different cultural conventions governing meetings\r\n\tparticipate efficiently in the different stages of the meeting process\r\n\tproduce the written documents associated with meetings and projects\r\n\tproduce and present a project proposal in spoken and written form\r\n\tcommunicate using different channels of communication (e.g. email, business letters)\r\n\tpresent data in various forms, including in visual form	The aim of the module is to practise the skills needed project work, including the language and culture of meetings and written correspondence.	Professional English: Projects and Meetings
26	fi			Ammatillista englantia: Projektit ja kokoukset
27	en	null	null	Optional Swedish Courses
27	fi			Ruotsin kielen valinnaiset opintojaksot
28	en			Advanced Swedish for Students of Law
28	fi	Opintojakson suoritettuaan opiskelija osaa:\r\n\r\n- esittää johtopäätöksiä oikeusalaansa liittyvissä kysymyksissä ja perustella käsityksiään\r\n- hankkia ja käyttää omaan oikeusalaansa liittyvää tietoa ruotsiksi\r\n- kuvata oman oikeusalansa ilmiöitä	Opintojakso vaaditaan KTM-tutkintoon, kun suuntautaumisvaihtoehtona ovat oikeustieteet. Muut opiskelijat voivat suorittaa opintojakson valinnaisena opintojaksona, mutta KTM-tutkintoa suorittavilla oikeustieteiden opiskelijoilla on etusija ryhmiin.	Juridisk svenska
29	en			My Field in Nordic Countries
29	fi	Tavoitteena on, että opiskelija osaa \r\n\r\nhankkia oman alansa ilmiöitä koskevaa tietoa pohjoismaisista tietolähteistä (esim. media, organisaatiot) \r\nkuvata, selittää ja vertailla oman alansa ilmiöitä Pohjoismaissa\r\ntoimia seminaarin järjestäjänä ja osallistujana		Min bransch i Norden
30	en	null	null	Optional Finnish Courses
30	fi		Useimpiin kandidaatin tutkintoihin sisältyy mm. yksi vieraan kielen neljän opintopisteen opintojakso taitotasoa B1-B2 sekä yksi kahden opintopisteen valinnainen opintojakso. Kahden opintopisteen opintojakso on yleensä opiskelijan vapaasti valittavissa, ja se voi olla myös soveltuva suomen kielen opintojakso.\r\n\r\nSeuraavassa on lueteltu kandidaatin tutkintoon soveltuvat suomen kielen valinnaiset opintojaksot (2 op). Opintojaksojen tarjonta saattaa vaihdella lukuvuosittain.	Suomen kielen valinnaiset opintojaksot
31	en			Thesis Writing in Finnish
31	fi	Opintojakson tavoitteena on, että opiskelija osaa\r\n\r\n\tmieltää tutkielmanteon kirjoittamisprosessina\r\n\tsoveltaa tieteellisen kirjoittamisen konventioita omaan tutkielmatekstiinsä\r\n\targumentoida tarkoituksenmukaisesti\r\n\tkäsitellä oman tutkimusaiheensa kannalta relevanttia eri lähteistä peräisin olevaa tietoa ja käyttää sitä omassa tekstissään \r\n\tmuokata tutkielmatekstiään saamansa palautteen mukaisesti\r\n\tarvioida omaansa ja muiden kirjoittamaa tieteellistä tekstiä\r\n\tsoveltaa kielenkäytön normeja ja viimeistellä tutkielmatekstinsä.		Kohti gradua
32	en			Writing Clinic
32	fi	Opintojakson tavoitteena on, että opiskelija \r\n\r\n hyödyntää saamaansa ohjausta kirjoittaessaan kandidaattitutkielmaansa \r\n osaa soveltaa tieteellisen kirjoittamisen konventioita omaan tekstiinsä\r\n argumentoi tarkoituksenmukaisesti\r\n osaa soveltaa kielenkäytön normeja ja viimeistellä tekstinsä.		Tekstiklinikka
33	en	null	null	Optional Spanish Courses
33	fi			Espanjan kielen valinnaiset opintojaksot
34	en			Introduction to Latin America Culture
34	fi	Opintojakson suoritettuaan opiskelija\r\n\r\n\ttuntee latinalaisamerikkalaista kulttuuria ja sen historiaa, kirjallisuutta, musiikkia, taidetta, perinteitä, nykypäivän elämää ja monikulttuurista yhteiskuntaa.\r\n\tymmärtää latinalaisamerikkalaisten ja espanjan kielen merkityksen maailmanlaajuisessa kulttuurienvälisessä kommunikaatiossa		Johdatus latinalaisen Amerikan kulttuuriin
35	en			Introduction to Spanish Culture
35	fi	Opintojakson suoritettuaan opiskelija\r\n\ttuntee Espanjan historiaa, kirjallisuutta, taidetta ja perinteitä\r\n\ttuntee nykypäivän espanjalaista elämää ja monikulttuurista yhteiskuntaa\r\n\ton tutustunut Espanjan ja sille kulttuurisesti läheisten maiden välisiin suhteisiin\r\n\tymmärtää espanjan kielen merkityksen maailmanlaajuisessa kulttuurienvälisessä kommunikaatiossa		Johdatus espanjalaiseen kulttuuriin
36	en			Spanish Oral and Written Communication
36	fi	Opintojakson suoritettuaan opiskelija\r\n\r\n\tpystyy ymmärtämään ja tuottamaan kirjallisia ja suullisia tekstejä melko hyvin\r\n\tpystyy suullisesti ja kirjallisesti ilmaisemaan itseään espanjaksi aiheissa, jotka häntä kiinnostavat\r\n\tymmärtää yleiskielistä asiatekstiä ja ajankohtaisia artikkeleita ja löytää niistä keskeisen sisällön\r\n\tosaa suullisesti ja kirjallisesti ilmaista mielipiteitä ja perustella niitä, kertoa itsestään ja ympäristöstään, koulutuksestaan, terveydentilastaan, matkoistaan ja muista arkipäivän tapahtumista.\r\n\tpystyy esittämään aktiivisesti kysymyksiä tai kommentteja ja vastaamaan kysymyksiin\r\n\ttunnistaa tekstin ymmärtämisen kannalta keskeiset rakenteet\r\n\tosaa kirjoittaa yksinkertaista tekstiä jokapäiväisistä asioista\r\n\tpystyy kommunikoimaan kulttuurisidonnaisesti, käyttäen kieleen ja kulttuuriin oleellisesti liittyvää etikettiä\r\n\r\nLisäksi syvennetään espanjalaisen kulttuurin tuntemusta erilaisten dokumenttien, artikkeleiden ja elokuvien avulla.	Opintojakso on yksiköiden vaatimusten mukainen vieraan kielen 4 opintopisteen opintojakso. Se sopii myös vapaavalintaiseksi kurssiksi kaikkien alojen opiskelijoille.	Espanjan kielen kirjallinen ja suullinen taito
37	en			Spanish Grammar for Advanced Students
38	en			Advanced Course in Spanish
38	fi	Opintojakson suoritettuaan opiskelija\r\n\r\n- tuntee eri tekstilajien ominaispiirteitä ja eroja\r\n- ymmärtää rakenteeltaan ja sisällöltään haastavia asiatekstejä eri aihepiireistä ja tunnistaa eri näkökulmia\r\n- osaa kirjoittaa oikeakielistä ja tyyliltään tarkoituksenmukaista tekstiä\r\n- kykenee kommunikoimaan sujuvasti ja luontevasti espanjaa äidinkielenään puhuvien kanssa\r\n- kykenee puolustamaan omia näkökantojaan\r\n- saavuttaa syventävät tiedot espanjan kieliopista\r\n- pystyy keskustelemaan ajankohtaisista teemoista	Opintojakso on yksiköiden vaatimusten mukainen valinnainen vieraan kielen kurssi. Se sopii myös vapaavalintaiseksi kurssiksi kaikkien alojen opiskelijoille.	Espanjan kielen syventävä kurssi
39	en	null	null	Optional French Courses
39	fi			Ranskan kielen valinnaiset opintojaksot
40	en			Advanced Course in French
40	fi	Opintojakson suoritettuaan opiskelija\r\n\ton harjaantunut  ymmärtämään kuultua ja kirjoitettua tekstiä eri medioista\r\n\tpystyy ilmaisemaan melko sujuvasti omia mielipiteitään ja perustelemaan niitä\r\n\tpystyy kirjoittamaan oman CV:n ja hakukirjeen sekä muita tekstejä huolitellusti\r\n\tpystyy suunnittelemaan ja pitämään esitelmän yhdessä pienen ryhmän kanssa \r\n\ttuntee ranskalaista korkeakoulujärjestelmää ja pärjää ranskankielisessä maailmassa	Opintojakso on yksiköiden vaatimusten mukainen vieraan kielen 4 opintopisteen opintojakso. Se sopii myös vapaavalintaiseksi kurssiksi kaikkien alojen opiskelijoille.	Cours de Français approfondi - ranskan syventävä kurssi
41	en			France yesterday, today and tomorrow
41	fi	Opintojakson suoritettuaan opiskelija\r\n\tymmärtää yleisluonteista asiatekstiä, osaa poimia siitä pääkohdat ja välittää mielipiteensä muille\r\n\tymmärtää jossakin määrin erikoisalojen tekstejä, tutustuu myös oman alansa teksteihin ja ymmärtää niistä olennaisen\r\n\tosaa ilmaista itseään ranskaksi  ja pystyy  pitämään ryhmässä pienen esitelmän ryhmän valitsemasta aiheesta\r\n\tosaa kirjoittaa yksinkertaista tekstiä aiheista, jotka ovat tuttuja tai henkilökohtaisesti kiinnostavia \r\n    osaa käyttää ranskankielistä kirjallista materiaalia omassa opiskelussaan ja  etsiä tarvitsemaansa tietoa netistä	Opintojakso on yksiköiden vaatimusten mukainen vieraan kielen 4 opintopisteen opintojakso. Se sopii myös vapaavalintaiseksi kurssiksi kaikkien alojen opiskelijoille.	Ranska eilen, tänään ja huomenna
42	en			Oral Practice in French
42	fi	Opintojakson suoritettuaan opiskelija \r\n\tpystyy ilmaisemaan itseään ranskaksi aiheissa, jotka häntä kiinnostavat \r\n\tpystyy esittämään kysymyksiä ja vastaamaan kysymyksiin\r\n\tpystyy kommunikoimaan kulttuurisidonnaisesti, käyttäen kieleen ja kulttuuriin oleellisesti liittyvää etikettiä	Opintojakso on yksiköiden vaatimusten mukainen vieraan kielen 2 opintopisteen opintojakso. Se sopii myös vapaavalintaiseksi kurssiksi kaikkien alojen opiskelijoille.	Ranskan kielen suullinen harjoituskurssi
43	en	null	null	Optional German Courses
43	fi			Saksan kielen valinnaiset opintojaksot
44	en			Advanced Course in German
44	fi	Opintojakson suoritettuaan opiskelija\r\n\r\n- tuntee eri tekstilajien ominaispiirteitä ja eroja\r\n- ymmärtää rakenteeltaan ja sisällöltään haastavia asiatekstejä eri aihepiireistä ja tunnistaa eri näkökulmia\r\n- osaa kirjoittaa oikeakielistä ja tyyliltään tarkoituksenmukaista tekstiä	Opintojakso sopii kaikkiin kandidaatintutkintoihin valinnaiseksi vieraan kielen opintojaksoksi sekä osaksi KTM-tutkinnon kieli- ja viestintäopintoja.	Lesen, Verstehen, Schreiben
45	en			Communicative Training II
45	fi	Opintojakson suoritettuaan\r\nopiskelija                                                                                                         \r\n- osaa kertoa omasta opiskelualastaan, tutkimusaiheestaan jne.                                                                 \r\n- tuntee työelämän suullisten viestintätilanteiden erityispiirteitä ja sanastoa ja osaa toimia niissä saksan kielellä (esim. puhelinkommunikaatio, neuvottelu)\r\n- osaa osallistua keskusteluun tai johtaa sitä\r\n- osaa pitää selkeän esityksen	Opintojakso sopii kaikkiin kandidaatintutkintoihin valinnaiseksi vieraan kielen opintojaksoksi sekä osaksi KTM-tutkinnon kieli- ja viestintäopintoja.	Kommunikationstraining II
46	en			Topical Texts in German
46	fi	Opintojakson suoritettuaan opiskelija\r\n- tuntee saksankielistä mediaa \r\n- tuntee oman alansa saksankielisiä tietolähteitä ja osaa hyödyntää niitä opiskelussaan ja työssään\r\n- ymmärtää yleisluonteista asiatekstiä ja (populaari)tieteellistä tekstiä ja osaa poimia niistä keskeisen sisällön\r\n- tunnistaa ja ymmärtää asiateksteille tyypillisiä rakenteita\r\n- osaa kertoa kirjallisesti omasta opiskelu- tai ammattialastaan\r\n- osaa hyödyntää kielellisiä apuneuvoja	Opintojakso on yksiköiden vaatimusten mukainen vieraan kielen 4 opintopisteen opintojakso. Se sopii myös vapaavalintaiseksi kurssiksi kaikkien alojen opiskelijoille.	Aktuelle Texte aus Presse und Studium
47	en			Communicative Training I
47	fi	Opintojakson suoritettuaan opiskelija\r\n\r\n- ymmärtää autenttisia jokapäiväiseen elämään liittyviä aiheita, esimerkiksi radiosta ja TV:stä \r\n- osaa laatia suullisen yhteenvedon kuulemastaan tai lukemastaan ja kuvailla esimerkiksi diagrammeja\r\n- osaa ilmaista ja perustella mielipiteitään	Opintojakso sopii kandidaatintutkintoihin valinnaiseksi vieraan kielen opintojaksoksi.	Kommunikationstraining I
48	en			German Oral and Written Skills
48	fi	Kurssin suoritettuaan opiskelija\r\n\r\n- ymmärtää selkeää peruskieltä, joka liittyy arkipäiväisiin aiheisiin (esim. asuminen, vapaa-aika)\r\n- osaa viestiä suullisissa ja kirjallisissa arki- ja opiskelutilanteissa saksankielisissä maissa (esim. virasto-asiointi, sähköpostit, luentojen seuraaminen)\r\n- tuntee suomalaisen ja saksalaisen yliopisto-opiskelun ja arkiviestinnän yhtäläisyyksiä ja eroja.	Opintojakso on yksiköiden vaatimusten mukainen vieraan kielen 4 opintopisteen opintojakso. Se sopii myös vapaavalintaiseksi kurssiksi kaikkien alojen opiskelijoille. Opintojakso sopii erityisesti opiskelijoille, jotka ovat kiinnostuneet arkipäivän kommunikaatiosta saksankielisissä maissa tai suunnittelevat vaihto-opiskelua.	Deutsch im Alltag und Studium
49	en	null	null	Optional Russian Courses
49	fi			Venäjän kielen valinnaiset opintojaksot
50	en			Oral Practice in Russian
50	fi	Opintojakson suoritettuaan opiskelija\r\n\r\n\tuskaltaa osallistua aktiivisesti kysymyksiä tai kommentteja esittäen jopa vähän muodollisempiinkin keskustelutilanteisiin (esim. ryhmätyö tai luento)\r\n\tosaa pitää valmistetun esityksen itseään kiinnostavasta aiheesta, niin että ottaa huomioon kuulijat ja vastaa heidän kysymyksiinsä.		Venäjän kielen suullinen harjoituskurssi
51	en			Reading and Talking in Russian
51	fi	Opintojakson suoritettuaan opiskelija\r\n\r\n- pystyy osallistumaan keskusteluun aiheesta, jonka kokee tärkeäksi\r\n- kykenee valmistauduttuaan pitämään (yksin tai jonkun kanssa) pienen alustuspuheenvuoron itseään kiinnostavasta aiheesta\r\n- osaa tuottaa yhtenäisen kirjallisen työn\r\n- osaa hakea itselleen tarpeellista tietoa, erityisesti verkosta, ja täydentää itsenäisesti sanavarastoaan.		Venäjän kielen luetunymmärtämis- ja keskustelukurssi
52	en			Basics of Speech Communication
52	fi	Opintojakson käytyään opiskelija\r\n\r\n- tuntee puheviestintään liittyviä käsitteitä ja ilmiöitä ja osaa soveltaa niitä erilaisissa vuorovaikutustilanteissa\r\n- tiedostaa omia vahvuuksiaan ja kehityskohteitaan viestijänä sekä osaa realistisesti arvioida vuorovaikutusosaamistaan ja haluaa kehittää sitä\r\n- osaa havainnoida ja kuunnella aktiivisesti ja analyyttisesti\r\n- osaa antaa, vastaanottaa ja käsitellä palautetta rakentavasti sekä hyödyntää saamaansa palautetta   \r\n- osaa valmistautua erilaisiin vuorovaikutustilanteisiin tarkoituksenmukaisesti eli esimerkiksi jäsentää, havainnollistaa, kohdentaa ja argumentoida viestinsä eri kuuntelijoille \r\n- tuntee ryhmän toiminnan periaatteet, osaa toimia yhteistyössä muiden kanssa ja osaa arvioida omaa sekä koko ryhmän toimintaa\r\n- ymmärtää vuorovaikutusosaamisen ja vuorovaikutussuhteiden merkityksen opiskelun, tieteellisen toiminnan ja työelämän kannalta\r\n- ymmärtää ja osaa ottaa huomioon viestinnän eettiset näkökulmat	Opintojakson tavoitteena on, että opiskelija ymmärtää vuorovaikutusosaamisen merkityksen osana akateemista asiantuntijuutta. Opintojaksolla perehdytään puheviestinnän ilmiöihin ja käsitteisiin sekä kehitetään omaa vuorovaikutusosaamista monipuolisten harjoitusten	Puheviestinnän perusteet
53	en	Upon successful completion of the course, students will\r\n\tknow the basics of academic reading and writing\r\n\tunderstand and critically evaluate sources, and express their own argumentation clearly and logically in appropriate academic/professional style\r\n\tbe able to communicate clearly and logically about their own field following appropriate academic/professional conventions\r\n\twork cooperatively and effectively in groups, negotiating and contributing to the completion of tasks\r\n\thave an understanding of English as a lingua franca and the influence of context and culture on communication\r\n\thave the necessary skills to evaluate and continue developing their English language and communication skills independently	The course aims to provide the student with core communication skills in English for academic purposes. These include understanding the conventions of English academic communication and applying critical thinking to reading, writing and speaking about research in English. To be completed during the first year of study.	Introduction to Academic English
53	fi			Johdatus akateemiseen englantiin
54	en			Academic Writing in Finnish
54	fi	Opintojakson tavoitteena on, että opiskelija osaa\r\n\r\n· soveltaa tieteellisen kirjoittamisen konventioita ja kehittää akateemisia tekstitaitojaan \r\n\r\n· argumentoida tarkoituksenmukaisesti ja käyttää eri lähteistä peräisin olevaa tietoa omassa tekstissään \r\n\r\n· antaa ja käyttää hyväkseen palautetta \r\n\r\n· arvioida tekstiä tietyn tekstilajin edustajana \r\n\r\n· laatia yleistajuista tekstiä tieteellisen tekstin pohjalta \r\n\r\n· soveltaa kielenkäytön normeja ja viimeistellä tekstinsä.		Tieteellinen kirjoittaminen
55	en			Swedish Oral and Written Communication
55	fi	Tavoitteena on, että opiskelija osaa viestiä tiettyä tarkoitusta varten asiantuntijana suullisissa ja kirjallisissa oman alansa tilanteissa niin, että hän ottaa huomioon viestintätilanteen tavoitteen, sisällön, osapuolet ja viestintätavan. \r\n\r\nOpiskelija osaa\r\n\r\n- kertoa itsestään, opiskelustaan ja omasta alastaan\r\n- kuvata, selittää ja määritellä oman alansa ilmiöitä\r\n- ilmaista ja perustella mielipiteitään ja käsityksiään oman alansa kysymyksistä\r\n- referoida ja viitata tietolähteisiin.	Opintojakso perustuu asetukseen yliopistojen tutkinnoista (794/2004). Tutkinnoissa vaadittava kielitaito määritellään oman alan kannalta tarpeelliseksi kielitaidoksi. Se vastaa samalla valtion henkilöstöltä kaksikielisessä viranomaisessa vaadittavaa kielitaitoa (ks. julkisyhteisöjen henkilöstöltä vaadittavasta kielitaidosta annettu laki 424/2003 sekä suomen ja ruotsin kielen taidon osoittamisesta valtionhallinnossa annettu asetus 481/2003).\r\n\r\nOpintojakso koostuu kirjallisesta viestinnästä (2 op) ja suullisesta viestinnästä (2 op).	Ruotsin kielen kirjallinen ja suullinen viestintä
56	en	After completing the basic studies students are familiar with the history of Western philosophy. They recognise the main subfields of philosophy and can describe their basic problems and concepts.	Basic studies provide students with an overview of the subfields of philosophy by means of the history of Western philosophy. They also give an introduction to logic and argumentation, as well as to the main concepts, trends and representatives of contemporary ontology, epistemology, philosophy of science, ethics, and social philosophy.	Basic Studies in Philosophy
56	fi	Filosofian perusopinnot suoritettuaan opiskelija tunnistaa filosofian osa-alueet ja niille ominaiset filosofiset ongelmat. Hän tuntee logiikan perusteet ja on kehittynyt kriittisen ajattelun taidossa. Hän hahmottaa ja osaa kuvata ontologian, tieto-opin ja tieteenfilosofian keskeiset lähestymistavat ja käsitteet. Hän tuntee etiikan ja yhteiskuntafilosofian tärkeimmät suuntaukset ja niiden edustajat.	Filosofian perusopinnot esittelevät filosofian osa-alueita länsimaisen filosofian historian kautta. Perusopinnoissa tutustutaan logiikan perusteisiin ja harjoitellaan argumenttien tunnistamista kirjoituksista. Lisäksi tutustutaan ontologian, tieto-opin, tieteenfilosofian, etiikan ja yhteiskuntafilosofian keskeisiin käsitteisiin, suuntauksiin ja niiden edustajiin.	Filosofian perusopinnot
58	en	The participants are capable of describing the central research topics, concepts, approaches, and arguments in epistemology and ontology. They are capable of identifying different conceptions of knowledge and their contexts, such as the standard analysis of knowledge and interpretational and social conceptions of knowledge. They are familiar with the most important theories of epistemic justification as well as philosophical theories of observations, and they are capable of reflecting on their shortcomings and virtues. They are aware of different conceptions of ontology and metaphysics so that they understand their philosophical questions and attempts to answer these questions. They are also capable of reflecting on these approaches in a critical way.		Epistemology and Ontology
58	fi	Opintojakson suoritettuaan opiskelija hahmottaa  ja osaa kuvata tieto-opin (eli epistemologian) ja ontologian keskeiset tutkimusaiheet, käsitteet, lähestymistavat ja argumentit. Hän tunnistaa erilaisia tiedon käsitteitä ja niiden käyttöyhteyksiä, kuten ns. klassisen tiedon käsitteen sekä tulkinnallisen ja sosiaalisen tiedon käsitteet.  Hän tuntee myös keskeisimmät havaintoa ja oikeutusta koskevat teoriat ja niitä koskevan kritiikin.  Hän ymmärtää ontologian ja metafysiikan käsitteiden erilaisia merkityksiä sekä hahmottaa yleisen metafysiikan eli ontologian keskeiset kysymykset ja niiden vastausyritykset ja osaa myös arvioida näitä vastauksia.		Tieto-oppi ja ontologia
59	en	The participants are capable of identifying the core areas in philosophy and their research problems. They understand how both the research problems and the nature of inquiry in philosophy differ from those in other academic disciplines. They are also capable of naming and describing the most important approaches and their representatives in the Western tradition of philosophy.		Introduction to Philosophy and Its History
59	fi	Opintojakson suoritettuaan opiskelija tunnistaa filosofian osa-alueet ja pääpiirteissään niille ominaiset filosofiset ongelmat. Hän ymmärtää, miten filosofiset ongelmat ja filosofisen tutkimuksen luonne eroavat muiden tieteenalojen tutkimusongelmista ja luonteesta. Hän osaa nimetä ja kuvata länsimaisen filosofian historian tärkeimpiä edustajia ja suuntauksia.		Johdatus filosofiaan ja sen historiaan
60	en	The participants have developed their skills in critical thinking. They are aware of different philosophical accounts of scientific knowledge and they are capable of reflecting on their shortcomings and virtues. They understand the nature of scientific knowledge so that they are able to describe the role of scientific reasoning, empirical evidence, and social practices in the making of scientific knowledge.		Argumentation and Philosophy of Science
60	fi	Opintojakson suoritettuaan opiskelija on kehittynyt kriittisen ajattelun taidossa, tuntee erilaisia vastauksia kysymykseen, mitä tieteellinen tieto on, ja osaa arvioida näitä vastauksia. Hän myös ymmärtää tieteellisen tiedon luonnetta, mikä ilmenee siinä, että hän osaa eritellä tieteellistä päättelyä, selvittää kokemusperäisen aineiston roolia tutkimuksessa ja kuvata tieteen sosiaalisia käytäntöjä.		Argumentaatio ja tieteenfilosofia
61	en	The participants are capable of reading and writing both propositional and predicate logic. They are familiar with the basic semantic notions, such as logical truth and logical consequence, and are thus able to evaluate the validity of inferences. They understand the main principles of deduction and proof and are capable of constructing simple deductions in the system of natural deduction.		Logic
61	fi	Opintojakson suoritettuaan opiskelija osaa lukea ja kirjoittaa lause- ja predikaattilogiikan kieliä ja on sisäistänyt semanttiset peruskäsitteet, erityisesti loogisen totuuden ja seurauksen käsitteet, niin että hän tunnistaa, ovatko teksteissä esiintyvät päätelmät totuuden säilyttäviä. Hän myös ymmärtää deduktiivisen päättelyn ja todistamisen idean, niin että hän osaa rakentaa yksinkertaisia deduktioita lauselogiikan luonnollisen päättelyn systeemissä.		Logiikka
62	en	After completing intermediate studies students understand the importance of the history of philosophy to all philosophizing. They are able to describe different periods and different approaches in the history of philosophy. They are familiar with the main trends in contemporary philosophy and have deepened their familiarity with logic, philosophy of science, and a few other subfields of philosophy. They are also familiar with research ethics. They are capable of writing a clear and coherent philosophical presentation and participating in philosophical discussion.	Intermediate studies in philosophy provide an overview of the history of philosophy and thus add to the students understanding of the main problems and concepts of the subfields of philosophy. They contain studies in contemporary philosophical logic, philosophy of science, and research ethics. In addition, they include study modules that deepen the student's familiarity with two other subfields of philosophy. The student also participates in the proseminar and the seminar and prepares a seminar essay and the Bachelors thesis.	Intermediate Studies in Philosophy
62	fi	Filosofian aineopinnot suoritettuaan opiskelija ymmärtää, että filosofian historian tunteminen on olennainen osa filosofian harjoittamista. Hän hahmottaa ja osaa kuvata filosofian historian eri kausia ja niille ominaisia lähestymistapoja. Hän tuntee nykyfilosofian keskeisimmät suuntaukset ja perusopintoja laajemmin logiikan, tieteenfilosofian ja joidenkin muiden filosofian osa-alueiden piirissä käytäviä keskusteluja. Hän on myös jonkin verran perehtynyt tutkimusetiikkaan. Hän osaa kirjoittaa käsittelytavaltaan selkeän ja johdonmukaisen  filosofisen esityksen ja osallistua filosofiseen keskusteluun.	Filosofian aineopinnot tarjoavat yleiskatsauksen filosofian historiaan ja näin laajentavat opiskelijan ymmärrystä filosofian eri osa-alueiden keskeisistä ongelmista ja käsitteistä. Aineopinnoissa perehdytään logiikan ja tieteenfilosofian nykysuuntauksiin sekä tutkimusetiikkaan.  Lisäksi opiskelija perehtyy kahteen muuhun filosofian osa-alueeseen perusopintoja syvällisemmin. Opiskelija osallistuu aktiivisesti proseminaariin  ja kandidaattiseminaariin sekä laatii seminaariesityksen ja kandidaatintutkielman.	Filosofian aineopinnot
63	en		Optional intermediate studies must include at least two study modules about the subdisciplines of Philosphy. 	Optional intermediate studies in Philosophy
63	fi	Opintojakson suoritettuaan opiskelija tuntee hyvin vähintään kahden filosofian osa-alueen keskeisiä kysymyksiä ja sisältöjä.	Valinnaiset aineopinnot koostuvat vähintään kahden filosofian osa-alueen aineopintotasoisista opinnoista.	Valinnaiset aineopinnot
64	en	After completing the course students are familiar with the core questions and the recent history of the philosophy of language. They are able to consider and assess the special features of various languages used in various contexts, such as ordinary language, artificial languages, the language of literature and religious language.		Philosophy of Language
64	fi	Opintojakson suoritettuaan opiskelija tuntee kielifilosofian keskeiset ongelmat ja lähihistorian. Hän osaa tarkastella ja arvioida erilaisissa yhteyksissä käytettyjen kielten, kuten arkikielen, keinotekoisten kielten, kirjallisuuden kielen ja uskonnollisen kielen, erityispiirteitä.		Kielifilosofia
65	en	The student has a good knowledge of some of the themes of modern social and political philosophy, that were introduced in the section YKYY5. S/he can describe and evaluate central themes of contemporary debates in social and political philosophy, and s/he can use them to analyze social and political problems philosophically.		Social Philosophy
65	fi	Opintojakson suoritettuaan opiskelija tuntee hyvin muutamia modernin yhteiskuntafilosofian keskeisiä kysymyksiä ja on siten syventänyt tietämystään teemoista, joihin hän tutustui yhteiskuntafilosofian perusopintojaksolla. Hän tuntee ja osaa kuvata nykykeskustelun keskeisiä aiheita ja teorioita ja pystyy näiden avulla tarkastelemaan yhteiskunnallisia kysymyksiä filosofisesti.		Yhteiskuntafilosofia
66	en	After completing the course students understand and are more able to analyze the basic problems of epistemology, which have been introduced in the basic course. They are capable of assessing epistemological views, connect them with discussions in other fields of philosophy and the special sciences and describe their specifically philosophical nature.		Epistemology
66	fi	Opintojakson suoritettuaan opiskelija tuntee syvemmin ja monipuolisemmin epistemologian perusongelmia, joihin hän on tutustunut tieto-opin perusopintojaksolla. Hän osaa kriittisesti arvioida erilaisia tieto-opillisia näkemyksiä,  kytkeä epistemologiset kysymykset muiden filosofian osa-alueiden ja erityistieteiden tarkasteluihin sekä tunnistaa ja kuvata niiden filosofisen erityisluonteen.		Epistemologia
67	en	The student has a good knowledge of some of the themes of modern ethics, that were introduced in the section YKYY5. S/he can describe and evaluate central themes of contemporary ethical debates and theories, and s/he can use them to analyze ethical problems philosophically.		Ethics
67	fi	Opintojakson suoritettuaan opiskelija tuntee hyvin muutamia modernin etiikan keskeisiä kysymyksiä ja siten syvemmin teemoja, joihin hän tutustui etiikan perusopintojaksolla. Hän tuntee ja osaa kuvata ja arvioida nykykeskustelun keskeisiä aiheita ja teorioita ja pystyy näiden avulla tarkastelemaan eettisiä ongelmia filosofian keinoin.		Etiikka
68	en	The student has a general overview of philosophy of culture and art, which are also needed in cultural and art critique.		Philosophy of Culture and Art
68	fi	Opintojakson suoritettuaan opiskelijalla on kattava yleiskuva kulttuuri- ja taidefilosofiasta, jota tarvitaan myös kulttuuri- ja taidekritiikin tekemiseen.		Kulttuuri- ja taiteenfilosofia
69	en	After completing the course students understand and are more able to analyze the basic problems of ontology, which have been introduced in the basic course. They have become familiar with the whole field of metaphysics, both to its traditional questions and to contemporary discussion. They are capable of critically evaluate various metaphysical views, connect them with the questions of other fields of philosophy and the special sciences and both recognize and describe their specifically philosophical nature.		Metaphysics and Ontology
69	fi	Opintojakson suoritettuaan opiskelija tuntee syvemmin ja monipuolisemmin ontologian perusongelmia, joihin hän on tutustunut ontologian perusopintojaksolla. Hän on laajentanut metafysiikan tuntemustaan koko metafysiikan alueelle, sekä sen perinteisiin kysymyksiin että nykykeskusteluun. Hän osaa kriittisesti arvioida erilaisia metafyysisiä näkemyksiä,  kytkeä metafyysiset kysymykset muiden filosofian osa-alueiden ja erityistieteiden tarkasteluihin sekä tunnistaa ja kuvata niiden filosofisen erityisluonteen.		Metafysiikka ja ontologia
70	en	After completing the course students are familiar with the main philosophical problems and theories of the mind, cognition, the body, and personhood. They are capable of describing and critically evaluating those problems and theories.		Philosophy of Mind
70	fi	Opintojakson suoritettuaan opiskelija tuntee keskeisimmät mieltä ja kognitiota sekä mieltä, ruumista ja persoonaa koskevat filosofiset ongelmat ja teoriat. Hän osaa esitellä ja arvioida niitä kriittisesti.		Mielen filosofia
71	en	After completing the study module students have come into contact with philosophical research and are capable of writing a clear and coherent thesis. To some extent, they are also capable of evaluating philosophical research and actively participating in philosophical discussion.		Seminar and Bachelor's Thesis
71	fi	Opintojakson suoritettuaan opiskelija on saanut kosketuksen filosofiseen tutkimustyöhön ja osaa laatia käsittelytavaltaan ja ilmaisultaan selkeän opinnäytteen. Hän osaa jossain määrin myös arvioida filosofista tutkimusta ja osallistua aktiivisesti filosofiseen keskusteluun.		Kandidaattiseminaari ja kandidaatintutkielma
72	en	The participants understand that good scientific practice is based both on epistemic and moral values. They are capable of describing how the so called constitutive values of science should be manifested in research processes and results, in the practices of scientific communities, and in the virtues of individual scientists. They understand that the so called contextual values of science can intervene in scientific inquiry in many ways.		Philosophy of Science and Research Ethics
72	fi	Opintojakson suoritettuaan opiskelija ymmärtää, että hyvä tieteellinen käytäntö perustuu sekä tiedollisiin että moraalisiin arvoihin. Hän hahmottaa ja osaa esittää, miten tieteelle ominaiset tiedolliset arvot luonnehtivat tutkimuksen tuloksia, tiedeyhteisön sosiaalisia käytäntöjä ja tieteentekijän hyveitä. Hän myös ymmärtää, miten eri tavoin tieteen käytäntöön nähden ulkoiset yhteiskunnalliset arvot voivat vaikuttaa tieteelliseen tutkimukseen.		Tieteenfilosofia ja tutkimusetiikka
73	en	The student understands why history of philosophy is a philosophical, not only a historical science. S/he knows historical formulations of central philosophical questions. S/he has notably an overview of classical questions concerning knowledge, ideas, truth, being, goodness, justice and beauty. S/he also knows different philosophical approaches such as empirism, rationalism, dialectics, or paragmatism. S/he understands the meaning and the specific use of classical sources in philosophy.		History of Philosophy
73	fi	Opintojakson suoritettuaan opiskelija ymmärtää, että filosofian historian harjoittaminen on filosofoimista, ei yksinomaan historiantutkimusta. Hän tuntee keskeiset filosofiset kysymykset ja lähestymistavat niiden tärkeimpien historiallisten muotoilujen kautta. Keskeisin tavoite on tiedollinen: opiskelijalla on opintojakson suoritettuaan yleiskuva filosofian tärkeimmistä klassisista kysymyksistä ja menetelmistä. Hän hahmottaa filosofian historiaa tietoa ja tietoisuutta, ideoita, olemista ja totuutta sekä hyvyyttä, oikeudenmukaisuutta ja kauneutta koskevien kysymysten kautta. Opintojakson suorittanut opiskelija tuntee erilaisia filosofisia lähestymistapoja, esimerkiksi dialektiikkaa, rationalismia, empirismiä, pragmatismia, ja osaa arvioida niitä. Hän ymmärtää alkuperäistekstien merkityksen filosofiassa sekä niiden käyttöön liittyviä erityisongelmia.		Filosofian historia
74	en	The students has a good understanding of one or two classical texts of philosophy. S/he knows how to read classical texts whose style and method can be very different from contemporary philosophy. S/he recognises historical problems and methods. The understanding of classical problematics provides him/her a deeper understanding of systematical questions analysed in other sections.		Classics of Philosophy
74	fi	Opintojakson suoritettuaan opiskelija tuntee hyvin yhden tai kaksi filosofian historian klassikkotekstiä. Hän osaa lukea, tarvittaessa kommentaarien avulla, klassisia tekstejä, joiden tyyli ja metodi saattavat poiketa paljonkin nykyfilosofiasta. Hän tunnistaa tekstissä käsiteltävän filosofisen ongelman ja siinä käytettävän metodin. Hän osaa klassikkotekstin parissa työskenneltyään taustoittaa ja syventää erilaisia systemaattisia kysymyksiä,  joita käsitellään muissa opintojaksoissa.		Klassikot
75	en	The participants are capable of writing clear and well argued philosophical texts, to give comments on philosophical papers and presentations and to discuss philosophical themes.		Proseminar
75	fi	Opintojakson suoritettuaan opiskelija osaa kirjoittaa käsittelytavaltaan ja ilmaisultaan selkeän filosofisen esityksen ja osallistua aktiivisesti filosofiseen keskusteluun.		Proseminaari
76	fi	Opintojakson suoritettuaan opiskelija tuntee 1900-luvun filosofian keskeiset suuntaukset ja pääpiirteissään myös niiden kulttuurisen ja yhteiskunnallisen ympäristön, niiden taustan modernissa filosofiassa sekä aseman ja keskinäisen vuorovaikutuksen nykyfilosofiassa.  Hän hahmottaa ja osaa kuvata ajatteluympäristön, josta myös nykyfilosofian systemaattiset kysymykset nousevat.		1900-luvun filosofia
77	en	After completing the course students are able to identify the extensions and the alternatives of classical logic. Particularly, they recognize modal notions and are capable of analyzing them by means of the notion of a possible world. They can formulate epistemological and metaphysical questions related to logic and evaluate answers given to those questions. They recognize the role of logic in philosophical views on theoretical rationality, i.e., the rationality of thought, and on practical rationality, i.e. the rationality of action.		Philosophical Logic
77	fi	Opintojakson suoritettuaan opiskelija tuntee ns. klassisen logiikan laajennukset ja vaihtoehdot. Erityisesti hän tunnistaa modaalikäsitteet ja osaa tarkastella niitä mahdollisen maailman käsitteen avulla. Hän osaa muotoilla logiikan epistemologisia ja metafyysisiä kysymyksiä ja arvioida niihin annettuja vastauksia. Hän tunnistaa logiikan paikan teoreettista eli inhimillisen ajattelun rationaalisuutta ja käytännöllistä eli inhimillisen toiminnan rationaalisuutta koskevissa filosofisissa näkemyksissä.		Filosofinen logiikka
78	en	A student of philosophy is familiar with some of the central problems, concepts and contents of other disciplines different from philosophy. 	Optional studies in the Bachelor's degree should include at least one composite study module (a minimum of 25 ETCS) offered by other degree programs.\r\n	Optional studies in Bachelor's programme
78	fi	Valinnaiset opinnot suoritettuaan filosofian opiskelija tuntee jossain määrin myös toisten tieteenalojen ongelmia, keskeisiä käsitteitä ja sisältöjä.	Kandidaatin tutkinnon valinnaisiin opintoihin on sisällytettävä ainakin yksi vähintään 20 op laajuinen opintokokonaisuus, joka voi olla jokin oman yksikön eri teema-alueista tai muiden tutkinto-ohjelmien tarjoamista valinnaisista opinnoista (joko omasta tai muista yksiköistä). \r\nMuiksi valinnaisiksi opinnoiksi on mahdollista valita myös lisää filosofian opintoja (kuten valinnaisia aineopintoja). On kuitenkin suositeltavaa suorittaa joko yksi laajempi opintokokonaisuus (60 op) tai (25 op:n kokonaisuuden lisäksi) muita vähintään 15 op:n opintokokonaisuuksia muista oppiaineista. \r\nValinnaiset opinnot voivat sisältää myös esim. vieraiden kielten, esiintymisen ja kirjoittamisen taitoja tai muita henkilökohtaisessa opintosuunnitelmassa hyödyllisiksi arvioituja erityisiä taitoja tai tietoja  kehittäviä opintosuorituksia.\r\n	Kandidaattiohjelman valinnaiset opinnot
79	en	After completing the Bachelor's Programme in philosophy  students are expected to recognize the main subfields of philosophy, to know the philosophical problems pertaining to those fields and to be familiar with the historical background of those problems. They understand how philosophical problems both differ from and are connected to the problems of other disciplines. Students are familiar with the basic concepts, trends and representatives of the subfields of philosophy. They are trained in logical inference, and they recognize and are able to analyze arguments in various texts, as well as to put forth their own arguments. They are capable of writing a scientific philosophical presentation and of taking part in philosophical discussion. To some extent, they are also familiar with the main problems, concepts and contents of another discipline or other disciplines they have chosen to study. They have basic skills in information technology, and skills in languages and communication sufficient for their future tasks, and they are familiar with the main concepts and problems of social sciences and humanities.	Studies in philosophy that belong to the Bachelor's Programme provide the students with an overview of the nature and the history of philosophical thought. They give an introduction to the basic concepts and views of the main subfields of philosophy. It is recommended that the student chooses a well considered and sufficiently wide combination of studies in other disciplines; those studies both support studies in philosophy and provide the student with wide competence that helps in building a career after graduating from the Master's Programme. The Bachelor's Thesis and the proseminar provide the students with the study skills that are needed in Master studies. The Bachelor's Programme also contains studies in languages and communication, career skills and information technology, the joint studies of the School of Social Sciences and Humanities, and studies in another discipline or other disciplines on the student's own choice.	Bachelor's Programme in Philosophy II
79	fi	Kandidaatin tutkinnon suoritettuaan opiskelija tunnistaa filosofian osa-alueet ja tuntee niille ominaiset filosofiset ongelmat sekä niiden historiallisen taustan. Hän ymmärtää, miten filosofiset ongelmat yhtäältä eroavat muiden tieteenalojen tutkimusongelmista ja toisaalta kytkeytyvät niihin.  Hän tuntee filosofian eri osa-alueiden keskeisiä käsitteitä, suuntauksia ja niiden edustajia. Opiskelija on harjaantunut loogisen päättelyn taidoissa, tunnistaa ja osaa analysoida argumentteja erilaisissa kirjoituksissa sekä esittää omia argumenttejaan. Hän osaa kirjoittaa käsittelytavaltaan filosofisen esityksen ja osallistua filosofiseen keskusteluun. Hän on valintansa mukaan perehtynyt myös jonkin verran toisen tieteenalan tai toisten tieteenalojen keskeisiin kysymyksiin, käsitteisiin ja sisältöihin. Hän hallitsee tietotekniset taidot, hänellä on opintojensa ja tulevien tehtäviensä kannalta riittävät kieli- ja viestintätaidot, ja hän tunnistaa  eri yhteiskunta- ja kulttuuritieteiden peruskäsitteitä ja keskeisiä ongelmia.	Kandidaatin tutkintoon sisältyvät filosofian opinnot antavat opiskelijalle yleiskuvan filosofisen ajattelun, sekä sen teoreettisten että käytännöllisten pääalueiden, luonteesta ja historiallisesta kehityksestä. Ne tutustuttavat filosofian keskeisimpien osa-alueiden peruskäsitteisiin ja näkemyksiin. Opiskelijan on suotavaa valita mielekäs ja riittävän laaja sivuaineopintojen kokonaisuus, joka yhtäältä tukee opiskelijan filosofista suuntautumista ja toisaalta antaa sellaista laaja-alaista osaamista, joka auttaa sijoittumista työelämään maisteriopintojen jälkeen. Kandidaattitutkielman ja proseminaarin tavoitteena on perehdyttää opiskelija maisteriopinnoissa vaadittaviin opiskelutaitoihin. Tutkintoon sisältyvät kieli- ja viestintäopinnot, urataitojen ja tietotekniikan opiskelua, yhteiskunta- ja kulttuuritieteiden yksikön yhteiset opinnot sekä filosofian opiskelijan oman valinnan mukaan toisen tieteenalan tai toisten tieteenalojen opintoja.	Filosofian kandidaattiohjelma II
80	en	null	null	Study skills
80	fi			Opiskeluvalmiudet
81	en			Introduction to Computing
81	fi	Opintojakson suoritettuaan\r\n- opiskelija tuntee yliopiston tietoteknisen ympäristön, osaa toimia mikroluokissa ja osaa käyttää yliopiston tarjoamia tietoteknisiä palveluita\r\n- opiskelijan osaa käyttää yleisimpiä sovellusohjelmistoja tehokkaasti ja monipuolisesti ja perussovellusten käyttötaidot ovat ajantasalla\r\n- opiskelijalla on valmiudet ylläpitää tietoteknisiä valmiuksiaan omatoimisesti		Tietotekniikka louhinnassa
82	en	The module aims at giving the students basic information on stdying in universities in general and in the University of Tampere in particular, and introduce them to planning their studies.\r\n\r\nDuring their first year of study, students write a personal study plan, which confirms the structure of the degree and combination of subjects the student will study. The student will be given guidance in writing the personal study plan by the major subject department and the ISSS.		Orientation and Personal Study Plan
82	fi	Opintojakson tarkoituksena on antaa opiskelijoille perustiedot yliopisto-opiskelusta yleensä ja erityisesti opiskelusta Tampereen yliopistossa, johdattaa oman tutkinto-ohjelman koulutustavoitteisiin ja opintojen suunnitteluun.\r\nOpiskelija laatii ensimmäisen opiskeluvuotensa aikana henkilökohtaisen opintosuunnitelman (HOPS), jonka yhteydessä varmistetaan muun muassa tutkinnon rakenne ja aineyhdistelmä. Henkilökohtainen opintosuunnitelma on ennen kaikkea opiskelijan oma opintojen suunnittelun apuväline, jonka laatimiseen hän saa ohjausta oman oppiaineensa opettajilta  sekä yksikön opintoneuvonnasta.		Johdatus louhintaan opiskelussa
83	en	null	null	Joint Studies / YKY
83	fi	Yhteiskunta- ja kulttuuritieteiden yksikön kaikkien tutkinto-ohjelmien opiskelijoille yhteiset opinnot suoritettuaan opiskelija osaa tulkita yhteiskuntaa ja kulttuuria ja hänellä on käsitys tutkimuksen suhteesta yhteiskuntaan, sen eri toimijoihin ja instituutioihin. \r\n\r\nOpiskelija ymmärtää yhteiskunta- ja kulttuuritieteellisen tutkimuksen tapoja muotoilla tutkimuksen kysymyksiä, tuntee tutkimuksen peruskäsitteitä ja on alustavasti perehtynyt määrällisen ja laadullisen tutkimuksen menetelmiin. Hän tunnistaa, ymmärtää ja osaa arvioida eri tavoin yhteiskunnasta ja ihmisistä kerättyä ja tuotettua tietoa sekä tietää, missä ja miten sitä kerätään ja missä sitä on saatavilla. \r\n\r\nOpiskelija tuntee Suomen lähihistorian keskeiset ilmiöt ja osaa eritellä niitä osana laajempaa eurooppalaista yhteiskuntakehitystä.  Hän tuntee etiikan ja yhteiskuntafilosofian tärkeimmät historialliset muotoilut sekä yleisimmät modernissa etiikassa ja yhteiskuntafilosofiassa esitetyt kuvaukset yksilön, yhteisön ja yhteiskunnan suhteista sekä niitä jäsentävästä kulttuurin käsitteestä. \r\n\r\nOpintokokonaisuuden käytyään opiskelija osaa sijoittaa oman tutkinto-ohjelmansa erityisen  aineksen osaksi laajempaa yhteiskunta- ja kulttuuritutkimuksen tieteenalojen kenttää. Hän on myös tutustunut yliopisto-opiskelussa sovellettaviin opetuksen muotoihin, opetusteknologioihin ja suoritustapoihin. 	Tutkintoon vaadittavien yhteisten opintojen määrä riippuu oman tutkinto-ohjelman vaatimuksista. Kaikkien tutkinto-ohjelmien opiskelijat suorittavat pakollisena opintojakson Yhteiskunnan tutkimuksen ajankohtaisia kysymyksiä.\r\n\r\nHistorian ja psykologian tutkinto-ohjelman opiskelijat valitsevat lisäksi neljästä yhteisten opintojen opintojaksosta kolme, logopedian tutkinto-ohjelman opiskelijat vastaavasti kaksi opintojaksoa. Yksikön muiden tutkinto-ohjelmien opiskelijat suorittavat yhteiset opinnot kokonaisuudessaan.	Yhteiskunta- ja kulttuuritieteiden yhteiset opinnot
84	en	The participants are aware of the most important historical approaches to ethics and social philosophy and of the principal modern conceptions of the relation between individual and community. They know the ethical and moral questions about goodness and justice. They are familiar with the ontological question of what human beings are in a community, and with the epistemological question of what human communities are as objects and subjects of knowledge. They are capable of reflecting on the philosophical assumptions implicit in contemporary moral and political debates.		Ethics and Social Philosophy
84	fi	Opintojakson suoritettuaan opiskelija tuntee etiikan ja yhteiskuntafilosofian tärkeimmät historialliset muotoilut sekä tärkeimmät modernissa etiikassa ja yhteiskuntafilosofiassa esitetyt kuvaukset yksilön, yhteisön ja yhteiskunnan suhteista sekä niitä jäsentävästä kulttuurin käsitteestä. Hän hahmottaa ennen kaikkea eettisen ja moraalisen kysymyksen siitä, mitä on hyvyys ja oikeudenmukaisuus yhteisössä. Hänen tuntee peruspiirteissään myös ontologisen kysymyksen siitä, mikä on ihminen yhteisössä, sekä epistemologisen kysymyksen ihmisyhteisöstä tiedon kohteena ja tuottajana. Hän osaa myös reflektoida eettisten, moraalisten ja yhteiskunnallisten kysymysten filosofisia taustaoletuksia.	Opintojakso vastaa kesään 2012 voimassa olleen opintosuunnitelman jaksoja seuraavasti:\r\n- YKYY5a Johdatus etiikkaan vastaa jaksoa PHILPE5 ja\r\n- YKYY5b Johdatus yhteiskuntafilosofiaan jaksoa PHILPE6.	Etiikka ja yhteiskuntafilosofia
85	en			History of Finnish Society from the 19th to 21th century
85	fi	Opintojakson suoritettuaan opiskelija tuntee ajan keskeiset ilmiöt ja osaa analysoida niitä osana laajempaa eurooppalaista yhteiskuntakehitystä. Opiskelija osaa kuvata aikakautta koskevien historiakäsitysten keskeisiä muutoksia.		Suomalaisen yhteiskunnan historia vuodesta 1809 nykyaikaan
86	en			Current Issues in Social Sciences
86	fi	Opiskelijalla on peruskäsitys yhteiskuntaa ja kulttuuria koskevan tutkimuksen suhteesta yhteiskuntaan, sen eri toimijoihin ja instituutioihin. Opiskelija ymmärtää esimerkkien avulla, miten eri ihmistieteiden alat jäsentävät nykymaailmaa, miten ne asettavat sitä koskevia kysymyksiä ja miten tutkimuksen tuottama tieto ja näkemykset syntyvät vastauksena näihin kysymyksiin.  Hän osaa alustavasti suhteuttaa ajankohtaista yhteiskuntakeskustelua tutkimustietoon, osaa erottaa tutkimuksen tiedonintressejä muiden toimijoiden näkökulmista ja oppii suhteuttamaan omaa ymmärrystään tutkimuksen tapaan käsitteellistää ajankohtaisia kysymyksiä. \r\n\r\nOpiskelija osaa etsiä opinnoissa tarvittavia kirjoja sekä tietyn aihepiirin painettuja ja elektronisia aineistoja (e-lehdet, e-kirjat) erilaisia tietokantapalveluita hyödyntäen. Opiskelija ymmärtää lähdekritiikin tärkeyden ja tietää tieteellisen viittauskäytännön periaatteet.		Yhteiskunnan tutkimuksen ajankohtaisia kysymyksiä
87	en			Action, Interpretation, and Knowledge
87	fi	Opiskelija hallitsee yhteiskunta- ja kulttuuritieteellisen tutkimuksen peruskäsitteitä, tuntee laadullisen tutkimuksen menetelmiä, osaa lukea ja arvioida laadullista tutkimusta sekä arvostaa erilaisia tiedon muotoja. Opiskelija ymmärtää ihmisten toiminnan ja kokemuksen tutkimuksen merkityksen. Hän osaa asettaa tiedon ja vallan väliseen suhteeseen liittyviä kysymyksiä ja keskustella niistä. Hän tunnistaa yhteiskunnallisten erojen ja hierarkioiden merkityksen yhteiskunnallisen tiedon tuotannossa ja ymmärtää yhteiskunnallisen marginalisaation, huono-osaisuuden ja epäoikeudenmukaisuuden kysymyksiä.		Toiminta, tulkinta ja tieto
88	en			Measuring society
88	fi	Kurssin tavoite on kehittää yhteiskuntaa koskevan määrällisen tiedon lukutaitoa. Opiskelija hallitsee perustasolla määrällisen tutkimuksen käsitteet sekä tuntee numeeristen aineistojen perustyypit ja rakentumisen tavat.  Hän osaa myös esittää tutkimustietoa koskevia kysymyksiä ja arvioida numerotiedon luotettavuutta. Opiskelija tunnistaa ja ymmärtää eri tavoin yhteiskunnasta ja ihmisistä kerättyä ja tuotettua tietoa sekä tietää, missä ja miten sitä kerätään ja missä sitä on saatavilla.		Yhteiskunnan mittaaminen
89	en			School of Social Sciences and Humanities, B.A.
98	en	After successful completion of the course the students will be able to\r\n\r\n use sources to research debate topics and prepare speeches\r\n construct arguments and use appropriate language in argumentative speech\r\n recognize and use rhetorical devices\r\n use language typical of debates\r\n give informative and persuasive speeches in a well-structured manner\r\n evaluate, analyze and react to arguments critically \r\n function in groups according to the roles prescribed by the debating rules\r\n control timing when giving speeches		Debating for Academic Purposes
110	fi	Opintojakson tavoitteena on, että opiskelija \r\n\r\n hyödyntää saamaansa ohjausta kirjoittaessaan kandidaattitutkielmaansa \r\n osaa soveltaa tieteellisen kirjoittamisen konventioita omaan tekstiinsä\r\n argumentoi tarkoituksenmukaisesti\r\n osaa soveltaa kielenkäytön normeja ja viimeistellä tekstinsä.		Tekstiklinikka
89	fi	Tutkintoihin kuuluvat akateemiset kieli- ja viestintäopinnot tukevat\r\n\r\n* oman alan opiskelua, alan kehityksen seuraamista ja aineistojen hyödyntämistä\r\n* kieli- ja viestintätaidon kehittämistä\r\n* asiantuntijuuden kehittymistä\r\n* tieteellisten käytäntöjen oppimista\r\n* kotimaisissa ja kansainvälisissä yhteyksissä toimimista\r\n* yksilön monikielisyyttä ja monikulttuurisuutta	Humanististen tieteiden kandidaatin tutkintoon kuuluvat seuraavat kieli- ja viestintäopinnot (15 op):\r\n\r\n- vieraan kielen opintojakso 4 op (taitotasolla B1-B2)\r\n- ruotsin kielen kirjallinen ja suullinen viestintä 4 op\r\n- tieteellinen kirjoittaminen 3 op\r\n- puheviestinnän perusteet 2 op\r\n- valinnainen opintojakso 2 op\r\n\r\nTutkintonsa valinnaiseksi opintojaksoksi opiskelija voi valita suomen, ruotsin tai vieraan kielen opintojakson taitotasolta B1-C2. Valinnaiset opintojaksot on kuvattu opetussuunnitelmassa kyseisten kielten kohdalla. Valinnaisten opintojaksojen tarjonta vaihtelee lukuvuosittain.\r\n\r\nVieraan kielen opintojakso (4 op) on tarkoitettu suoritettavaksi 1. opintovuonna, ja ruotsin kielen kirjallisen ja suullisen viestinnän opintojakso (4 op) 2. opintovuonna. Suomen kielen tieteellisen kirjoittamisen opintojakso on tarkoitus suorittaa 3. opintovuonna.\r\n\r\nEdellä mainittujen kieli- ja viestintäopintojen (15 op) lisäksi opiskelija voi suorittaa myös muita kielikeskuksen tarjoamia kieli- ja viestintäopintoja ja sisällyttää ne tutkinnon vapaavalintaisiin opintoihin.\r\n	Yhteiskunta- ja kulttuuritieteiden yksikkö, HuK, kieli- ja viestintäopinnot
90	en	null	null	Optional Courses
90	fi		Useimpiin kandidaatin tutkintoihin kuuluu yhteensä kuusi opintopistettä vieraan kielen opintoja: yksi 4 opintopisteen opintojakso sekä yksi 2 opintopisteen valinnainen opintojakso. Opiskelija voi suorittaa tutkintoonsa myös kaksi 4 opintopisteen vieraan kielen opintojaksoa (esim. ranskaa 4 op ja saksaa 4 op). Tällöin molempien opintojaksojen on oltava vähintään taitotasoa B1, englannin opintojakson vähintään taitotasoa B2. Kieli- ja viestintäopintoja on tällöin yhteensä 17 opintopistettä.\r\n\r\nTutkintoihin soveltuvat vieraiden kielten sekä suomen ja ruotsin kielen valinnaiset opintojaksot on listattu alla. Opintojaksot löytyvät myös kyseisten kielten kohdalta.	Valinnaiset opintojaksot
91	en			Optional English Courses
91	fi		Useimpiin kandidaatin tutkintoihin voi sisältyä yhteensä kuusi opintopistettä vieraan kielen opintoja: yksi neljän opintopisteen opintojakso taitotasoa B1-B2 sekä yksi kahden opintopisteen opintojakso taitotasoa B1-C2. Opintojaksot ovat yleensä opiskelijan vapaasti valittavissa.\r\n\r\nSeuraavassa on lueteltu kandidaatin tutkintoon soveltuvat englannin kielen valinnaiset opintojaksot (2 op). Opintojaksojen tarjonta saattaa vaihdella lukuvuosittain. KTM-tutkintoa suorittavilla oikeustieteiden opiskelijoilla on etusija Legal English -kurssille.	Englannin kielen valinnaiset opintojaksot
92	en	On completion of the course students are expected to:\r\n\r\n- be able to understand demanding academic and professional texts relating to their field;\r\n- be able to discuss issues relating to their own field with both colleagues and laymen;\r\n- be able to give a presentation relating to their academic field making use of suitable technology; be able to deal with questions that may arise in the course of a presentation and to lead a discussion arising from a presentation;\r\n-be able to write texts typical for written communication in professional life;	Students will  widen and deepen their knowledge of legal English so that they are able to communicate effectively in working life.	Legal English
92	fi			Englannin kielen syventävä kurssi oikeustieteilijöille
93	en	On completion of the course students are expected to:\r\n\r\n-be aware of the typical conventions of academic seminars and texts;\r\n-be able to understand the differences between statements based on facts, opinions and feelings;\r\n-be able to participate actively in seminar in all the possible roles (defender, opponent, chair or group member);\r\n-be able to construct a justified argumentation chain by expressing thoughts logically and presenting relevant evidence;\r\n-be able to hold a seminar presentation;\r\n-be able to write a presentation abstract;	The purpose of the course is to raise the level of students skills in academic English so that they can participate actively in an international academic seminar.	Seminar Skills
93	fi			Tieteellisen seminaarin englanti
94	en	Students will \r\n\r\n-\tproduce a body of original writing in English.\r\n-\tproduce an analysis of their own writing in which they explain their objectives and how they achieved them.\r\n-\tlearn how to review creative writing critically and to discuss their critical evaluations clearly.\r\n-\tdevelop their self-critical and self-editing skills.\r\n\r\nIn addition, students will be encouraged to publish their work online and to organize and participate in a public reading of work produced on the course.	The aim of the course is for students to develop their powers of expression in written English via the medium of creative writing and to understand how they can produce various responses in the reader.	Creative Writing in English
94	fi			Luova kirjoittaminen englanniksi
95	en	On completion of the course, students are expected to be able to:\r\n\r\n\tprepare and deliver a discipline-specific, academic presentation that follows accepted norms and good academic practice\r\n\twrite an abstract\r\n\tfind and use resources to facilitate and support the above\r\n\tpresent information clearly and convincingly\r\n\tcommunicate with confidence within both academic and workplace contexts \r\n\twork effectively in groups, negotiating and contributing to the completion of the tasks	The course focuses on giving a presentation and writing an abstract.\r\nStudents will also improve their oral communication skills in academic and professional situations relating to their own field and future profession. Students will become familiar with the conventions and cultural considerations associated with spoken production and interaction. In addition, students will develop communication confidence and team working skills.	Advanced Academic Presentations
95	fi			Akateemiset esitelmät
96	en	After successful completion of the course the students will\r\n\r\n be able to understand and know how to overcome barriers to effective communication\r\n have knowledge and tools to reflect on and develop their own communication skills\r\n be able to use strategies to enhance dialogue and constructive communication at work	The course aims to provide students with such communication skills that help further dialogue in the work place when using English as a lingua franca.	Talk at Work: Dialogue
96	fi			Työelämäviestintä
97	en	Upon successful completion of the course, students will be able to\r\n\tunderstand and apply the basics of written communication in their work\r\n\twrite documents related to work life professional writing\r\n\tuse appropriate terminology of their professional field \r\n\tfollow the conventions of academic and professional written communication\r\n\tconsider cultural aspects in professional writing	The course aims to provide the student with English professional writing competencies needed in working life.	Professional Writing
97	fi			Työelämän tekstit
98	fi			Väittelytaidon kurssi
111	fi			Espanjan kielen valinnaiset opintojaksot
99	en	Upon successful completion of the course, students will be able to\r\n\tuse various cohesive devices to improve the flow of their text\r\n\tknow and employ a formal academic style (grammar and vocabulary)\r\n\texpress their own voice in their writing\r\n\tidentify the structure, moves and linguistic features of a research article\r\n\twrite a data commentary and/or a literature review\r\n\tconstruct a research paper on a topic in their area of study	The course aims to provide the student with the competencies to write a well-structured and stylistically appropriate research article in English with a view to eventual publication.	Writing for Research
99	fi			Tutkimusviestintä
100	en	On completion of the course, students are expected to be able to:\r\n- use some participatory methods successfully in English\r\n- have a good insight into the types of support necessary for successful collaboration and co-creation in English\r\n- design, plan and facilitate a short participatory workshop session in English\r\n- reflect on and assess the impact of your personal style in facilitation and communication in English\r\n- reflect on and assess your ability to collaborate in a multidisciplinary group in English	Today's fast-changing and complex working environments require us to engage in multiple styles of leading and learning. The ability to facilitate processes, collaborate and co-create in multidisciplinary /multiprofessional teams and use participatory methods to engage others are core academic / professional skills of university graduates. In this course, students will experience and experiment with a broad range of participatory methodologies and tools to facilitate group processes in English.	Facilitating, Engaging and Co-creating
100	fi			Fasilitointi, osallistavuus ja yhteiskehittely
101	en	On completion of the course, students are expected to be able to\r\n-\tdevelop a deeper understanding of the role of English in academic texts\r\n-\tlearn to identify the distinctiveness of academic English\r\n-\tapply the knowledge acquired in the course to  written assignments and whereby engage with the conventions of academic writing in their discipline	Academic discourse and practice has been informed by discourse analysis, narratology and numerous other approaches. This interdisciplinary course examines several of these approaches in various disciplines in order to get a fuller understanding of the role of discourse in academic texts and research.\r\nThe readings dealt with in the course will be reviewed and the contents of the articles discussed paying particular attention to the actual discourse of academic articles. Students will select a particular methodology/ approach/theory and apply it to their area of specialization.	Academic Discourse in Theory and Practice
101	fi			Akateeminen diskurssi teoriassa ja käytännössä
102	en	Upon successful completion of the course, students will\r\n\thave a better understanding of the influence of culture on language and communication\r\n\thave practiced and developed their intercultural communication skills is real situations with people from different cultural backgrounds\r\n\tbe able to evaluate and plan the development of their own intercultural communication skills\r\n\tbe more aware of cultural difference	The aim of the course is to become more aware of the influence of culture on language and communication, and to improve intercultural communication skills through discussion and interaction in mixed groups of Finnish and international students. The group work part of the course will be integrated with Cultural Conversations in the Intercultural Communication Studies program.	Intercultural Communication
102	fi			Kulttuurienvälinen viestintä
103	en	Negotiated with the participating students.	The aim of the module is to assist the participating students in identifying their own academic and/or professional learning outcomes (in groups and/or individually), to help them structure a course of independent study to meet those outcomes, to provide ongoing guidance and support, and finally to assess with them the results of their learning.	Academic/Professional English: Independent Learning Module
103	fi			Akateemista ja ammatillista englantia: itsenäinen oppimismoduuli
104	en	Upon successful completion of the course, students will be able to\r\n\tunderstand the different cultural conventions governing meetings\r\n\tparticipate efficiently in the different stages of the meeting process\r\n\tproduce the written documents associated with meetings and projects\r\n\tproduce and present a project proposal in spoken and written form\r\n\tcommunicate using different channels of communication (e.g. email, business letters)\r\n\tpresent data in various forms, including in visual form	The aim of the module is to practise the skills needed project work, including the language and culture of meetings and written correspondence.	Professional English: Projects and Meetings
104	fi			Ammatillista englantia: Projektit ja kokoukset
105	en	null	null	Optional Swedish Courses
105	fi			Ruotsin kielen valinnaiset opintojaksot
106	en			Advanced Swedish for Students of Law
106	fi	Opintojakson suoritettuaan opiskelija osaa:\r\n\r\n- esittää johtopäätöksiä oikeusalaansa liittyvissä kysymyksissä ja perustella käsityksiään\r\n- hankkia ja käyttää omaan oikeusalaansa liittyvää tietoa ruotsiksi\r\n- kuvata oman oikeusalansa ilmiöitä	Opintojakso vaaditaan KTM-tutkintoon, kun suuntautaumisvaihtoehtona ovat oikeustieteet. Muut opiskelijat voivat suorittaa opintojakson valinnaisena opintojaksona, mutta KTM-tutkintoa suorittavilla oikeustieteiden opiskelijoilla on etusija ryhmiin.	Juridisk svenska
107	en			My Field in Nordic Countries
107	fi	Tavoitteena on, että opiskelija osaa \r\n\r\nhankkia oman alansa ilmiöitä koskevaa tietoa pohjoismaisista tietolähteistä (esim. media, organisaatiot) \r\nkuvata, selittää ja vertailla oman alansa ilmiöitä Pohjoismaissa\r\ntoimia seminaarin järjestäjänä ja osallistujana		Min bransch i Norden
108	en	null	null	Optional Finnish Courses
108	fi		Useimpiin kandidaatin tutkintoihin sisältyy mm. yksi vieraan kielen neljän opintopisteen opintojakso taitotasoa B1-B2 sekä yksi kahden opintopisteen valinnainen opintojakso. Kahden opintopisteen opintojakso on yleensä opiskelijan vapaasti valittavissa, ja se voi olla myös soveltuva suomen kielen opintojakso.\r\n\r\nSeuraavassa on lueteltu kandidaatin tutkintoon soveltuvat suomen kielen valinnaiset opintojaksot (2 op). Opintojaksojen tarjonta saattaa vaihdella lukuvuosittain.	Suomen kielen valinnaiset opintojaksot
109	en			Thesis Writing in Finnish
109	fi	Opintojakson tavoitteena on, että opiskelija osaa\r\n\r\n\tmieltää tutkielmanteon kirjoittamisprosessina\r\n\tsoveltaa tieteellisen kirjoittamisen konventioita omaan tutkielmatekstiinsä\r\n\targumentoida tarkoituksenmukaisesti\r\n\tkäsitellä oman tutkimusaiheensa kannalta relevanttia eri lähteistä peräisin olevaa tietoa ja käyttää sitä omassa tekstissään \r\n\tmuokata tutkielmatekstiään saamansa palautteen mukaisesti\r\n\tarvioida omaansa ja muiden kirjoittamaa tieteellistä tekstiä\r\n\tsoveltaa kielenkäytön normeja ja viimeistellä tutkielmatekstinsä.		Kohti gradua
110	en			Writing Clinic
111	en	null	null	Optional Spanish Courses
112	fi	Opintojakson suoritettuaan opiskelija\r\n\r\n\ttuntee latinalaisamerikkalaista kulttuuria ja sen historiaa, kirjallisuutta, musiikkia, taidetta, perinteitä, nykypäivän elämää ja monikulttuurista yhteiskuntaa.\r\n\tymmärtää latinalaisamerikkalaisten ja espanjan kielen merkityksen maailmanlaajuisessa kulttuurienvälisessä kommunikaatiossa		Johdatus latinalaisen Amerikan kulttuuriin
113	en			Introduction to Spanish Culture
113	fi	Opintojakson suoritettuaan opiskelija\r\n\ttuntee Espanjan historiaa, kirjallisuutta, taidetta ja perinteitä\r\n\ttuntee nykypäivän espanjalaista elämää ja monikulttuurista yhteiskuntaa\r\n\ton tutustunut Espanjan ja sille kulttuurisesti läheisten maiden välisiin suhteisiin\r\n\tymmärtää espanjan kielen merkityksen maailmanlaajuisessa kulttuurienvälisessä kommunikaatiossa		Johdatus espanjalaiseen kulttuuriin
114	en			Spanish Oral and Written Communication
114	fi	Opintojakson suoritettuaan opiskelija\r\n\r\n\tpystyy ymmärtämään ja tuottamaan kirjallisia ja suullisia tekstejä melko hyvin\r\n\tpystyy suullisesti ja kirjallisesti ilmaisemaan itseään espanjaksi aiheissa, jotka häntä kiinnostavat\r\n\tymmärtää yleiskielistä asiatekstiä ja ajankohtaisia artikkeleita ja löytää niistä keskeisen sisällön\r\n\tosaa suullisesti ja kirjallisesti ilmaista mielipiteitä ja perustella niitä, kertoa itsestään ja ympäristöstään, koulutuksestaan, terveydentilastaan, matkoistaan ja muista arkipäivän tapahtumista.\r\n\tpystyy esittämään aktiivisesti kysymyksiä tai kommentteja ja vastaamaan kysymyksiin\r\n\ttunnistaa tekstin ymmärtämisen kannalta keskeiset rakenteet\r\n\tosaa kirjoittaa yksinkertaista tekstiä jokapäiväisistä asioista\r\n\tpystyy kommunikoimaan kulttuurisidonnaisesti, käyttäen kieleen ja kulttuuriin oleellisesti liittyvää etikettiä\r\n\r\nLisäksi syvennetään espanjalaisen kulttuurin tuntemusta erilaisten dokumenttien, artikkeleiden ja elokuvien avulla.	Opintojakso on yksiköiden vaatimusten mukainen vieraan kielen 4 opintopisteen opintojakso. Se sopii myös vapaavalintaiseksi kurssiksi kaikkien alojen opiskelijoille.	Espanjan kielen kirjallinen ja suullinen taito
115	en			Spanish Grammar for Advanced Students
115	fi	Kurssi on tarkotettu opiskelijoille, joilla on hyvä espanjan kielen taito, mutta jotka haluavat syventää kielioppitaitojaan.	.	Espanjan kielioppia edistyneille
116	en			Advanced Course in Spanish
116	fi	Opintojakson suoritettuaan opiskelija\r\n\r\n- tuntee eri tekstilajien ominaispiirteitä ja eroja\r\n- ymmärtää rakenteeltaan ja sisällöltään haastavia asiatekstejä eri aihepiireistä ja tunnistaa eri näkökulmia\r\n- osaa kirjoittaa oikeakielistä ja tyyliltään tarkoituksenmukaista tekstiä\r\n- kykenee kommunikoimaan sujuvasti ja luontevasti espanjaa äidinkielenään puhuvien kanssa\r\n- kykenee puolustamaan omia näkökantojaan\r\n- saavuttaa syventävät tiedot espanjan kieliopista\r\n- pystyy keskustelemaan ajankohtaisista teemoista	Opintojakso on yksiköiden vaatimusten mukainen valinnainen vieraan kielen kurssi. Se sopii myös vapaavalintaiseksi kurssiksi kaikkien alojen opiskelijoille.	Espanjan kielen syventävä kurssi
117	en	null	null	Optional French Courses
117	fi			Ranskan kielen valinnaiset opintojaksot
118	en			Advanced Course in French
118	fi	Opintojakson suoritettuaan opiskelija\r\n\ton harjaantunut  ymmärtämään kuultua ja kirjoitettua tekstiä eri medioista\r\n\tpystyy ilmaisemaan melko sujuvasti omia mielipiteitään ja perustelemaan niitä\r\n\tpystyy kirjoittamaan oman CV:n ja hakukirjeen sekä muita tekstejä huolitellusti\r\n\tpystyy suunnittelemaan ja pitämään esitelmän yhdessä pienen ryhmän kanssa \r\n\ttuntee ranskalaista korkeakoulujärjestelmää ja pärjää ranskankielisessä maailmassa	Opintojakso on yksiköiden vaatimusten mukainen vieraan kielen 4 opintopisteen opintojakso. Se sopii myös vapaavalintaiseksi kurssiksi kaikkien alojen opiskelijoille.	Cours de Français approfondi - ranskan syventävä kurssi
119	en			France yesterday, today and tomorrow
119	fi	Opintojakson suoritettuaan opiskelija\r\n\tymmärtää yleisluonteista asiatekstiä, osaa poimia siitä pääkohdat ja välittää mielipiteensä muille\r\n\tymmärtää jossakin määrin erikoisalojen tekstejä, tutustuu myös oman alansa teksteihin ja ymmärtää niistä olennaisen\r\n\tosaa ilmaista itseään ranskaksi  ja pystyy  pitämään ryhmässä pienen esitelmän ryhmän valitsemasta aiheesta\r\n\tosaa kirjoittaa yksinkertaista tekstiä aiheista, jotka ovat tuttuja tai henkilökohtaisesti kiinnostavia \r\n    osaa käyttää ranskankielistä kirjallista materiaalia omassa opiskelussaan ja  etsiä tarvitsemaansa tietoa netistä	Opintojakso on yksiköiden vaatimusten mukainen vieraan kielen 4 opintopisteen opintojakso. Se sopii myös vapaavalintaiseksi kurssiksi kaikkien alojen opiskelijoille.	Ranska eilen, tänään ja huomenna
120	en			Oral Practice in French
120	fi	Opintojakson suoritettuaan opiskelija \r\n\tpystyy ilmaisemaan itseään ranskaksi aiheissa, jotka häntä kiinnostavat \r\n\tpystyy esittämään kysymyksiä ja vastaamaan kysymyksiin\r\n\tpystyy kommunikoimaan kulttuurisidonnaisesti, käyttäen kieleen ja kulttuuriin oleellisesti liittyvää etikettiä	Opintojakso on yksiköiden vaatimusten mukainen vieraan kielen 2 opintopisteen opintojakso. Se sopii myös vapaavalintaiseksi kurssiksi kaikkien alojen opiskelijoille.	Ranskan kielen suullinen harjoituskurssi
121	en	null	null	Optional German Courses
121	fi			Saksan kielen valinnaiset opintojaksot
122	en			Advanced Course in German
122	fi	Opintojakson suoritettuaan opiskelija\r\n\r\n- tuntee eri tekstilajien ominaispiirteitä ja eroja\r\n- ymmärtää rakenteeltaan ja sisällöltään haastavia asiatekstejä eri aihepiireistä ja tunnistaa eri näkökulmia\r\n- osaa kirjoittaa oikeakielistä ja tyyliltään tarkoituksenmukaista tekstiä	Opintojakso sopii kaikkiin kandidaatintutkintoihin valinnaiseksi vieraan kielen opintojaksoksi sekä osaksi KTM-tutkinnon kieli- ja viestintäopintoja.	Lesen, Verstehen, Schreiben
123	en			Communicative Training II
123	fi	Opintojakson suoritettuaan\r\nopiskelija                                                                                                         \r\n- osaa kertoa omasta opiskelualastaan, tutkimusaiheestaan jne.                                                                 \r\n- tuntee työelämän suullisten viestintätilanteiden erityispiirteitä ja sanastoa ja osaa toimia niissä saksan kielellä (esim. puhelinkommunikaatio, neuvottelu)\r\n- osaa osallistua keskusteluun tai johtaa sitä\r\n- osaa pitää selkeän esityksen	Opintojakso sopii kaikkiin kandidaatintutkintoihin valinnaiseksi vieraan kielen opintojaksoksi sekä osaksi KTM-tutkinnon kieli- ja viestintäopintoja.	Kommunikationstraining II
124	en			Topical Texts in German
124	fi	Opintojakson suoritettuaan opiskelija\r\n- tuntee saksankielistä mediaa \r\n- tuntee oman alansa saksankielisiä tietolähteitä ja osaa hyödyntää niitä opiskelussaan ja työssään\r\n- ymmärtää yleisluonteista asiatekstiä ja (populaari)tieteellistä tekstiä ja osaa poimia niistä keskeisen sisällön\r\n- tunnistaa ja ymmärtää asiateksteille tyypillisiä rakenteita\r\n- osaa kertoa kirjallisesti omasta opiskelu- tai ammattialastaan\r\n- osaa hyödyntää kielellisiä apuneuvoja	Opintojakso on yksiköiden vaatimusten mukainen vieraan kielen 4 opintopisteen opintojakso. Se sopii myös vapaavalintaiseksi kurssiksi kaikkien alojen opiskelijoille.	Aktuelle Texte aus Presse und Studium
125	en			Communicative Training I
125	fi	Opintojakson suoritettuaan opiskelija\r\n\r\n- ymmärtää autenttisia jokapäiväiseen elämään liittyviä aiheita, esimerkiksi radiosta ja TV:stä \r\n- osaa laatia suullisen yhteenvedon kuulemastaan tai lukemastaan ja kuvailla esimerkiksi diagrammeja\r\n- osaa ilmaista ja perustella mielipiteitään	Opintojakso sopii kandidaatintutkintoihin valinnaiseksi vieraan kielen opintojaksoksi.	Kommunikationstraining I
126	en			German Oral and Written Skills
126	fi	Kurssin suoritettuaan opiskelija\r\n\r\n- ymmärtää selkeää peruskieltä, joka liittyy arkipäiväisiin aiheisiin (esim. asuminen, vapaa-aika)\r\n- osaa viestiä suullisissa ja kirjallisissa arki- ja opiskelutilanteissa saksankielisissä maissa (esim. virasto-asiointi, sähköpostit, luentojen seuraaminen)\r\n- tuntee suomalaisen ja saksalaisen yliopisto-opiskelun ja arkiviestinnän yhtäläisyyksiä ja eroja.	Opintojakso on yksiköiden vaatimusten mukainen vieraan kielen 4 opintopisteen opintojakso. Se sopii myös vapaavalintaiseksi kurssiksi kaikkien alojen opiskelijoille. Opintojakso sopii erityisesti opiskelijoille, jotka ovat kiinnostuneet arkipäivän kommunikaatiosta saksankielisissä maissa tai suunnittelevat vaihto-opiskelua.	Deutsch im Alltag und Studium
127	en	null	null	Optional Russian Courses
127	fi			Venäjän kielen valinnaiset opintojaksot
128	en			Oral Practice in Russian
128	fi	Opintojakson suoritettuaan opiskelija\r\n\r\n\tuskaltaa osallistua aktiivisesti kysymyksiä tai kommentteja esittäen jopa vähän muodollisempiinkin keskustelutilanteisiin (esim. ryhmätyö tai luento)\r\n\tosaa pitää valmistetun esityksen itseään kiinnostavasta aiheesta, niin että ottaa huomioon kuulijat ja vastaa heidän kysymyksiinsä.		Venäjän kielen suullinen harjoituskurssi
129	en			Reading and Talking in Russian
129	fi	Opintojakson suoritettuaan opiskelija\r\n\r\n- pystyy osallistumaan keskusteluun aiheesta, jonka kokee tärkeäksi\r\n- kykenee valmistauduttuaan pitämään (yksin tai jonkun kanssa) pienen alustuspuheenvuoron itseään kiinnostavasta aiheesta\r\n- osaa tuottaa yhtenäisen kirjallisen työn\r\n- osaa hakea itselleen tarpeellista tietoa, erityisesti verkosta, ja täydentää itsenäisesti sanavarastoaan.		Venäjän kielen luetunymmärtämis- ja keskustelukurssi
130	en			Basics of Speech Communication
130	fi	Opintojakson käytyään opiskelija\r\n\r\n- tuntee puheviestintään liittyviä käsitteitä ja ilmiöitä ja osaa soveltaa niitä erilaisissa vuorovaikutustilanteissa\r\n- tiedostaa omia vahvuuksiaan ja kehityskohteitaan viestijänä sekä osaa realistisesti arvioida vuorovaikutusosaamistaan ja haluaa kehittää sitä\r\n- osaa havainnoida ja kuunnella aktiivisesti ja analyyttisesti\r\n- osaa antaa, vastaanottaa ja käsitellä palautetta rakentavasti sekä hyödyntää saamaansa palautetta   \r\n- osaa valmistautua erilaisiin vuorovaikutustilanteisiin tarkoituksenmukaisesti eli esimerkiksi jäsentää, havainnollistaa, kohdentaa ja argumentoida viestinsä eri kuuntelijoille \r\n- tuntee ryhmän toiminnan periaatteet, osaa toimia yhteistyössä muiden kanssa ja osaa arvioida omaa sekä koko ryhmän toimintaa\r\n- ymmärtää vuorovaikutusosaamisen ja vuorovaikutussuhteiden merkityksen opiskelun, tieteellisen toiminnan ja työelämän kannalta\r\n- ymmärtää ja osaa ottaa huomioon viestinnän eettiset näkökulmat	Opintojakson tavoitteena on, että opiskelija ymmärtää vuorovaikutusosaamisen merkityksen osana akateemista asiantuntijuutta. Opintojaksolla perehdytään puheviestinnän ilmiöihin ja käsitteisiin sekä kehitetään omaa vuorovaikutusosaamista monipuolisten harjoitusten	Puheviestinnän perusteet
131	en	Upon successful completion of the course, students will\r\n\tknow the basics of academic reading and writing\r\n\tunderstand and critically evaluate sources, and express their own argumentation clearly and logically in appropriate academic/professional style\r\n\tbe able to communicate clearly and logically about their own field following appropriate academic/professional conventions\r\n\twork cooperatively and effectively in groups, negotiating and contributing to the completion of tasks\r\n\thave an understanding of English as a lingua franca and the influence of context and culture on communication\r\n\thave the necessary skills to evaluate and continue developing their English language and communication skills independently	The course aims to provide the student with core communication skills in English for academic purposes. These include understanding the conventions of English academic communication and applying critical thinking to reading, writing and speaking about research in English. To be completed during the first year of study.	Introduction to Academic English
131	fi			Johdatus akateemiseen englantiin
132	en			Academic Writing in Finnish
132	fi	Opintojakson tavoitteena on, että opiskelija osaa\r\n\r\n· soveltaa tieteellisen kirjoittamisen konventioita ja kehittää akateemisia tekstitaitojaan \r\n\r\n· argumentoida tarkoituksenmukaisesti ja käyttää eri lähteistä peräisin olevaa tietoa omassa tekstissään \r\n\r\n· antaa ja käyttää hyväkseen palautetta \r\n\r\n· arvioida tekstiä tietyn tekstilajin edustajana \r\n\r\n· laatia yleistajuista tekstiä tieteellisen tekstin pohjalta \r\n\r\n· soveltaa kielenkäytön normeja ja viimeistellä tekstinsä.		Tieteellinen kirjoittaminen
133	en			Swedish Oral and Written Communication
133	fi	Tavoitteena on, että opiskelija osaa viestiä tiettyä tarkoitusta varten asiantuntijana suullisissa ja kirjallisissa oman alansa tilanteissa niin, että hän ottaa huomioon viestintätilanteen tavoitteen, sisällön, osapuolet ja viestintätavan. \r\n\r\nOpiskelija osaa\r\n\r\n- kertoa itsestään, opiskelustaan ja omasta alastaan\r\n- kuvata, selittää ja määritellä oman alansa ilmiöitä\r\n- ilmaista ja perustella mielipiteitään ja käsityksiään oman alansa kysymyksistä\r\n- referoida ja viitata tietolähteisiin.	Opintojakso perustuu asetukseen yliopistojen tutkinnoista (794/2004). Tutkinnoissa vaadittava kielitaito määritellään oman alan kannalta tarpeelliseksi kielitaidoksi. Se vastaa samalla valtion henkilöstöltä kaksikielisessä viranomaisessa vaadittavaa kielitaitoa (ks. julkisyhteisöjen henkilöstöltä vaadittavasta kielitaidosta annettu laki 424/2003 sekä suomen ja ruotsin kielen taidon osoittamisesta valtionhallinnossa annettu asetus 481/2003).\r\n\r\nOpintojakso koostuu kirjallisesta viestinnästä (2 op) ja suullisesta viestinnästä (2 op).	Ruotsin kielen kirjallinen ja suullinen viestintä
134	en	After completing the basic studies students are familiar with the history of Western philosophy. They recognise the main subfields of philosophy and can describe their basic problems and concepts.	Basic studies provide students with an overview of the subfields of philosophy by means of the history of Western philosophy. They also give an introduction to logic and argumentation, as well as to the main concepts, trends and representatives of contemporary ontology, epistemology, philosophy of science, ethics, and social philosophy.	Basic Studies in Philosophy
134	fi	Filosofian perusopinnot suoritettuaan opiskelija tunnistaa filosofian osa-alueet ja niille ominaiset filosofiset ongelmat. Hän tuntee logiikan perusteet ja on kehittynyt kriittisen ajattelun taidossa. Hän hahmottaa ja osaa kuvata ontologian, tieto-opin ja tieteenfilosofian keskeiset lähestymistavat ja käsitteet. Hän tuntee etiikan ja yhteiskuntafilosofian tärkeimmät suuntaukset ja niiden edustajat.	Filosofian perusopinnot esittelevät filosofian osa-alueita länsimaisen filosofian historian kautta. Perusopinnoissa tutustutaan logiikan perusteisiin ja harjoitellaan argumenttien tunnistamista kirjoituksista. Lisäksi tutustutaan ontologian, tieto-opin, tieteenfilosofian, etiikan ja yhteiskuntafilosofian keskeisiin käsitteisiin, suuntauksiin ja niiden edustajiin.	Filosofian perusopinnot
136	en	The participants are capable of describing the central research topics, concepts, approaches, and arguments in epistemology and ontology. They are capable of identifying different conceptions of knowledge and their contexts, such as the standard analysis of knowledge and interpretational and social conceptions of knowledge. They are familiar with the most important theories of epistemic justification as well as philosophical theories of observations, and they are capable of reflecting on their shortcomings and virtues. They are aware of different conceptions of ontology and metaphysics so that they understand their philosophical questions and attempts to answer these questions. They are also capable of reflecting on these approaches in a critical way.		Epistemology and Ontology
136	fi	Opintojakson suoritettuaan opiskelija hahmottaa  ja osaa kuvata tieto-opin (eli epistemologian) ja ontologian keskeiset tutkimusaiheet, käsitteet, lähestymistavat ja argumentit. Hän tunnistaa erilaisia tiedon käsitteitä ja niiden käyttöyhteyksiä, kuten ns. klassisen tiedon käsitteen sekä tulkinnallisen ja sosiaalisen tiedon käsitteet.  Hän tuntee myös keskeisimmät havaintoa ja oikeutusta koskevat teoriat ja niitä koskevan kritiikin.  Hän ymmärtää ontologian ja metafysiikan käsitteiden erilaisia merkityksiä sekä hahmottaa yleisen metafysiikan eli ontologian keskeiset kysymykset ja niiden vastausyritykset ja osaa myös arvioida näitä vastauksia.		Tieto-oppi ja ontologia
137	en	The participants are capable of identifying the core areas in philosophy and their research problems. They understand how both the research problems and the nature of inquiry in philosophy differ from those in other academic disciplines. They are also capable of naming and describing the most important approaches and their representatives in the Western tradition of philosophy.		Introduction to Philosophy and Its History
137	fi	Opintojakson suoritettuaan opiskelija tunnistaa filosofian osa-alueet ja pääpiirteissään niille ominaiset filosofiset ongelmat. Hän ymmärtää, miten filosofiset ongelmat ja filosofisen tutkimuksen luonne eroavat muiden tieteenalojen tutkimusongelmista ja luonteesta. Hän osaa nimetä ja kuvata länsimaisen filosofian historian tärkeimpiä edustajia ja suuntauksia.		Johdatus filosofiaan ja sen historiaan
138	en	The participants have developed their skills in critical thinking. They are aware of different philosophical accounts of scientific knowledge and they are capable of reflecting on their shortcomings and virtues. They understand the nature of scientific knowledge so that they are able to describe the role of scientific reasoning, empirical evidence, and social practices in the making of scientific knowledge.		Argumentation and Philosophy of Science
138	fi	Opintojakson suoritettuaan opiskelija on kehittynyt kriittisen ajattelun taidossa, tuntee erilaisia vastauksia kysymykseen, mitä tieteellinen tieto on, ja osaa arvioida näitä vastauksia. Hän myös ymmärtää tieteellisen tiedon luonnetta, mikä ilmenee siinä, että hän osaa eritellä tieteellistä päättelyä, selvittää kokemusperäisen aineiston roolia tutkimuksessa ja kuvata tieteen sosiaalisia käytäntöjä.		Argumentaatio ja tieteenfilosofia
139	en	The participants are capable of reading and writing both propositional and predicate logic. They are familiar with the basic semantic notions, such as logical truth and logical consequence, and are thus able to evaluate the validity of inferences. They understand the main principles of deduction and proof and are capable of constructing simple deductions in the system of natural deduction.		Logic
139	fi	Opintojakson suoritettuaan opiskelija osaa lukea ja kirjoittaa lause- ja predikaattilogiikan kieliä ja on sisäistänyt semanttiset peruskäsitteet, erityisesti loogisen totuuden ja seurauksen käsitteet, niin että hän tunnistaa, ovatko teksteissä esiintyvät päätelmät totuuden säilyttäviä. Hän myös ymmärtää deduktiivisen päättelyn ja todistamisen idean, niin että hän osaa rakentaa yksinkertaisia deduktioita lauselogiikan luonnollisen päättelyn systeemissä.		Logiikka
140	en	After completing intermediate studies students understand the importance of the history of philosophy to all philosophizing. They are able to describe different periods and different approaches in the history of philosophy. They are familiar with the main trends in contemporary philosophy and have deepened their familiarity with logic, philosophy of science, and a few other subfields of philosophy. They are also familiar with research ethics. They are capable of writing a clear and coherent philosophical presentation and participating in philosophical discussion.	Intermediate studies in philosophy provide an overview of the history of philosophy and thus add to the students understanding of the main problems and concepts of the subfields of philosophy. They contain studies in contemporary philosophical logic, philosophy of science, and research ethics. In addition, they include study modules that deepen the student's familiarity with two other subfields of philosophy. The student also participates in the proseminar and the seminar and prepares a seminar essay and the Bachelors thesis.	Intermediate Studies in Philosophy
150	en	The participants understand that good scientific practice is based both on epistemic and moral values. They are capable of describing how the so called constitutive values of science should be manifested in research processes and results, in the practices of scientific communities, and in the virtues of individual scientists. They understand that the so called contextual values of science can intervene in scientific inquiry in many ways.		Philosophy of Science and Research Ethics
140	fi	Filosofian aineopinnot suoritettuaan opiskelija ymmärtää, että filosofian historian tunteminen on olennainen osa filosofian harjoittamista. Hän hahmottaa ja osaa kuvata filosofian historian eri kausia ja niille ominaisia lähestymistapoja. Hän tuntee nykyfilosofian keskeisimmät suuntaukset ja perusopintoja laajemmin logiikan, tieteenfilosofian ja joidenkin muiden filosofian osa-alueiden piirissä käytäviä keskusteluja. Hän on myös jonkin verran perehtynyt tutkimusetiikkaan. Hän osaa kirjoittaa käsittelytavaltaan selkeän ja johdonmukaisen  filosofisen esityksen ja osallistua filosofiseen keskusteluun.	Filosofian aineopinnot tarjoavat yleiskatsauksen filosofian historiaan ja näin laajentavat opiskelijan ymmärrystä filosofian eri osa-alueiden keskeisistä ongelmista ja käsitteistä. Aineopinnoissa perehdytään logiikan ja tieteenfilosofian nykysuuntauksiin sekä tutkimusetiikkaan.  Lisäksi opiskelija perehtyy kahteen muuhun filosofian osa-alueeseen perusopintoja syvällisemmin. Opiskelija osallistuu aktiivisesti proseminaariin  ja kandidaattiseminaariin sekä laatii seminaariesityksen ja kandidaatintutkielman.	Filosofian aineopinnot
141	en		Optional intermediate studies must include at least two study modules about the subdisciplines of Philosphy. 	Optional intermediate studies in Philosophy
141	fi	Opintojakson suoritettuaan opiskelija tuntee hyvin vähintään kahden filosofian osa-alueen keskeisiä kysymyksiä ja sisältöjä.	Valinnaiset aineopinnot koostuvat vähintään kahden filosofian osa-alueen aineopintotasoisista opinnoista.	Valinnaiset aineopinnot
142	en	After completing the course students are familiar with the core questions and the recent history of the philosophy of language. They are able to consider and assess the special features of various languages used in various contexts, such as ordinary language, artificial languages, the language of literature and religious language.		Philosophy of Language
142	fi	Opintojakson suoritettuaan opiskelija tuntee kielifilosofian keskeiset ongelmat ja lähihistorian. Hän osaa tarkastella ja arvioida erilaisissa yhteyksissä käytettyjen kielten, kuten arkikielen, keinotekoisten kielten, kirjallisuuden kielen ja uskonnollisen kielen, erityispiirteitä.		Kielifilosofia
143	en	The student has a good knowledge of some of the themes of modern social and political philosophy, that were introduced in the section YKYY5. S/he can describe and evaluate central themes of contemporary debates in social and political philosophy, and s/he can use them to analyze social and political problems philosophically.		Social Philosophy
143	fi	Opintojakson suoritettuaan opiskelija tuntee hyvin muutamia modernin yhteiskuntafilosofian keskeisiä kysymyksiä ja on siten syventänyt tietämystään teemoista, joihin hän tutustui yhteiskuntafilosofian perusopintojaksolla. Hän tuntee ja osaa kuvata nykykeskustelun keskeisiä aiheita ja teorioita ja pystyy näiden avulla tarkastelemaan yhteiskunnallisia kysymyksiä filosofisesti.		Yhteiskuntafilosofia
144	en	After completing the course students understand and are more able to analyze the basic problems of epistemology, which have been introduced in the basic course. They are capable of assessing epistemological views, connect them with discussions in other fields of philosophy and the special sciences and describe their specifically philosophical nature.		Epistemology
144	fi	Opintojakson suoritettuaan opiskelija tuntee syvemmin ja monipuolisemmin epistemologian perusongelmia, joihin hän on tutustunut tieto-opin perusopintojaksolla. Hän osaa kriittisesti arvioida erilaisia tieto-opillisia näkemyksiä,  kytkeä epistemologiset kysymykset muiden filosofian osa-alueiden ja erityistieteiden tarkasteluihin sekä tunnistaa ja kuvata niiden filosofisen erityisluonteen.		Epistemologia
145	en	The student has a good knowledge of some of the themes of modern ethics, that were introduced in the section YKYY5. S/he can describe and evaluate central themes of contemporary ethical debates and theories, and s/he can use them to analyze ethical problems philosophically.		Ethics
145	fi	Opintojakson suoritettuaan opiskelija tuntee hyvin muutamia modernin etiikan keskeisiä kysymyksiä ja siten syvemmin teemoja, joihin hän tutustui etiikan perusopintojaksolla. Hän tuntee ja osaa kuvata ja arvioida nykykeskustelun keskeisiä aiheita ja teorioita ja pystyy näiden avulla tarkastelemaan eettisiä ongelmia filosofian keinoin.		Etiikka
146	en	The student has a general overview of philosophy of culture and art, which are also needed in cultural and art critique.		Philosophy of Culture and Art
146	fi	Opintojakson suoritettuaan opiskelijalla on kattava yleiskuva kulttuuri- ja taidefilosofiasta, jota tarvitaan myös kulttuuri- ja taidekritiikin tekemiseen.		Kulttuuri- ja taiteenfilosofia
147	en	After completing the course students understand and are more able to analyze the basic problems of ontology, which have been introduced in the basic course. They have become familiar with the whole field of metaphysics, both to its traditional questions and to contemporary discussion. They are capable of critically evaluate various metaphysical views, connect them with the questions of other fields of philosophy and the special sciences and both recognize and describe their specifically philosophical nature.		Metaphysics and Ontology
147	fi	Opintojakson suoritettuaan opiskelija tuntee syvemmin ja monipuolisemmin ontologian perusongelmia, joihin hän on tutustunut ontologian perusopintojaksolla. Hän on laajentanut metafysiikan tuntemustaan koko metafysiikan alueelle, sekä sen perinteisiin kysymyksiin että nykykeskusteluun. Hän osaa kriittisesti arvioida erilaisia metafyysisiä näkemyksiä,  kytkeä metafyysiset kysymykset muiden filosofian osa-alueiden ja erityistieteiden tarkasteluihin sekä tunnistaa ja kuvata niiden filosofisen erityisluonteen.		Metafysiikka ja ontologia
148	en	After completing the course students are familiar with the main philosophical problems and theories of the mind, cognition, the body, and personhood. They are capable of describing and critically evaluating those problems and theories.		Philosophy of Mind
148	fi	Opintojakson suoritettuaan opiskelija tuntee keskeisimmät mieltä ja kognitiota sekä mieltä, ruumista ja persoonaa koskevat filosofiset ongelmat ja teoriat. Hän osaa esitellä ja arvioida niitä kriittisesti.		Mielen filosofia
149	en	After completing the study module students have come into contact with philosophical research and are capable of writing a clear and coherent thesis. To some extent, they are also capable of evaluating philosophical research and actively participating in philosophical discussion.		Seminar and Bachelor's Thesis
149	fi	Opintojakson suoritettuaan opiskelija on saanut kosketuksen filosofiseen tutkimustyöhön ja osaa laatia käsittelytavaltaan ja ilmaisultaan selkeän opinnäytteen. Hän osaa jossain määrin myös arvioida filosofista tutkimusta ja osallistua aktiivisesti filosofiseen keskusteluun.		Kandidaattiseminaari ja kandidaatintutkielma
150	fi	Opintojakson suoritettuaan opiskelija ymmärtää, että hyvä tieteellinen käytäntö perustuu sekä tiedollisiin että moraalisiin arvoihin. Hän hahmottaa ja osaa esittää, miten tieteelle ominaiset tiedolliset arvot luonnehtivat tutkimuksen tuloksia, tiedeyhteisön sosiaalisia käytäntöjä ja tieteentekijän hyveitä. Hän myös ymmärtää, miten eri tavoin tieteen käytäntöön nähden ulkoiset yhteiskunnalliset arvot voivat vaikuttaa tieteelliseen tutkimukseen.		Tieteenfilosofia ja tutkimusetiikka
151	en	The student understands why history of philosophy is a philosophical, not only a historical science. S/he knows historical formulations of central philosophical questions. S/he has notably an overview of classical questions concerning knowledge, ideas, truth, being, goodness, justice and beauty. S/he also knows different philosophical approaches such as empirism, rationalism, dialectics, or paragmatism. S/he understands the meaning and the specific use of classical sources in philosophy.		History of Philosophy
151	fi	Opintojakson suoritettuaan opiskelija ymmärtää, että filosofian historian harjoittaminen on filosofoimista, ei yksinomaan historiantutkimusta. Hän tuntee keskeiset filosofiset kysymykset ja lähestymistavat niiden tärkeimpien historiallisten muotoilujen kautta. Keskeisin tavoite on tiedollinen: opiskelijalla on opintojakson suoritettuaan yleiskuva filosofian tärkeimmistä klassisista kysymyksistä ja menetelmistä. Hän hahmottaa filosofian historiaa tietoa ja tietoisuutta, ideoita, olemista ja totuutta sekä hyvyyttä, oikeudenmukaisuutta ja kauneutta koskevien kysymysten kautta. Opintojakson suorittanut opiskelija tuntee erilaisia filosofisia lähestymistapoja, esimerkiksi dialektiikkaa, rationalismia, empirismiä, pragmatismia, ja osaa arvioida niitä. Hän ymmärtää alkuperäistekstien merkityksen filosofiassa sekä niiden käyttöön liittyviä erityisongelmia.		Filosofian historia
152	en	The students has a good understanding of one or two classical texts of philosophy. S/he knows how to read classical texts whose style and method can be very different from contemporary philosophy. S/he recognises historical problems and methods. The understanding of classical problematics provides him/her a deeper understanding of systematical questions analysed in other sections.		Classics of Philosophy
152	fi	Opintojakson suoritettuaan opiskelija tuntee hyvin yhden tai kaksi filosofian historian klassikkotekstiä. Hän osaa lukea, tarvittaessa kommentaarien avulla, klassisia tekstejä, joiden tyyli ja metodi saattavat poiketa paljonkin nykyfilosofiasta. Hän tunnistaa tekstissä käsiteltävän filosofisen ongelman ja siinä käytettävän metodin. Hän osaa klassikkotekstin parissa työskenneltyään taustoittaa ja syventää erilaisia systemaattisia kysymyksiä,  joita käsitellään muissa opintojaksoissa.		Klassikot
153	en	The participants are capable of writing clear and well argued philosophical texts, to give comments on philosophical papers and presentations and to discuss philosophical themes.		Proseminar
153	fi	Opintojakson suoritettuaan opiskelija osaa kirjoittaa käsittelytavaltaan ja ilmaisultaan selkeän filosofisen esityksen ja osallistua aktiivisesti filosofiseen keskusteluun.		Proseminaari
154	en	The student knows the principal philosophical currents of 20th century philosophy. S/he is also aware of their cultural and social context, of their background in modern philosophy, and of their interaction. The aim is also to understand the theoretical background of contemporary systematic questions.		Twentieth Century Philosophy
154	fi	Opintojakson suoritettuaan opiskelija tuntee 1900-luvun filosofian keskeiset suuntaukset ja pääpiirteissään myös niiden kulttuurisen ja yhteiskunnallisen ympäristön, niiden taustan modernissa filosofiassa sekä aseman ja keskinäisen vuorovaikutuksen nykyfilosofiassa.  Hän hahmottaa ja osaa kuvata ajatteluympäristön, josta myös nykyfilosofian systemaattiset kysymykset nousevat.		1900-luvun filosofia
155	en	After completing the course students are able to identify the extensions and the alternatives of classical logic. Particularly, they recognize modal notions and are capable of analyzing them by means of the notion of a possible world. They can formulate epistemological and metaphysical questions related to logic and evaluate answers given to those questions. They recognize the role of logic in philosophical views on theoretical rationality, i.e., the rationality of thought, and on practical rationality, i.e. the rationality of action.		Philosophical Logic
155	fi	Opintojakson suoritettuaan opiskelija tuntee ns. klassisen logiikan laajennukset ja vaihtoehdot. Erityisesti hän tunnistaa modaalikäsitteet ja osaa tarkastella niitä mahdollisen maailman käsitteen avulla. Hän osaa muotoilla logiikan epistemologisia ja metafyysisiä kysymyksiä ja arvioida niihin annettuja vastauksia. Hän tunnistaa logiikan paikan teoreettista eli inhimillisen ajattelun rationaalisuutta ja käytännöllistä eli inhimillisen toiminnan rationaalisuutta koskevissa filosofisissa näkemyksissä.		Filosofinen logiikka
156	en	A student of philosophy is familiar with some of the central problems, concepts and contents of other disciplines different from philosophy. 	Optional studies in the Bachelor's degree should include at least one composite study module (a minimum of 25 ETCS) offered by other degree programs.\r\n	Optional studies in Bachelor's programme
156	fi	Valinnaiset opinnot suoritettuaan filosofian opiskelija tuntee jossain määrin myös toisten tieteenalojen ongelmia, keskeisiä käsitteitä ja sisältöjä.	Kandidaatin tutkinnon valinnaisiin opintoihin on sisällytettävä ainakin yksi vähintään 20 op laajuinen opintokokonaisuus, joka voi olla jokin oman yksikön eri teema-alueista tai muiden tutkinto-ohjelmien tarjoamista valinnaisista opinnoista (joko omasta tai muista yksiköistä). \r\nMuiksi valinnaisiksi opinnoiksi on mahdollista valita myös lisää filosofian opintoja (kuten valinnaisia aineopintoja). On kuitenkin suositeltavaa suorittaa joko yksi laajempi opintokokonaisuus (60 op) tai (25 op:n kokonaisuuden lisäksi) muita vähintään 15 op:n opintokokonaisuuksia muista oppiaineista. \r\nValinnaiset opinnot voivat sisältää myös esim. vieraiden kielten, esiintymisen ja kirjoittamisen taitoja tai muita henkilökohtaisessa opintosuunnitelmassa hyödyllisiksi arvioituja erityisiä taitoja tai tietoja  kehittäviä opintosuorituksia.\r\n	Kandidaattiohjelman valinnaiset opinnot
\.


--
-- Data for Name: studycomponent_type; Type: TABLE DATA; Schema: public; Owner: degree
--

COPY studycomponent_type (id, sc_type) FROM stdin;
2	studyModule
0	degreeProgramme
1	courseUnit
\.


--
-- Name: studycomponent_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: degree
--

SELECT pg_catalog.setval('studycomponent_type_id_seq', 1, false);


--
-- Data for Name: user_component_mount; Type: TABLE DATA; Schema: public; Owner: degree
--

COPY user_component_mount (id, id_user, id_component_where, id_component_what) FROM stdin;
1	1	78	43
\.


--
-- Name: user_component_mount_id_seq; Type: SEQUENCE SET; Schema: public; Owner: degree
--

SELECT pg_catalog.setval('user_component_mount_id_seq', 1, true);


--
-- Data for Name: user_credits; Type: TABLE DATA; Schema: public; Owner: degree
--

COPY user_credits (id, grade, grade_date, id_user, id_idea) FROM stdin;
1	3	2014-05-22	1	38817
2	4	2014-05-22	1	40657
3	2	2014-05-22	1	30131
4	2	2014-05-22	1	39008
5	1	2014-05-22	1	38780
6	3	2014-05-22	1	38784
7	2	2014-05-22	1	38800
8	4	2014-05-22	1	38812
9	4	2014-05-22	1	40856
10	3	2014-05-22	1	40851
\.


--
-- Name: user_credits_id_seq; Type: SEQUENCE SET; Schema: public; Owner: degree
--

SELECT pg_catalog.setval('user_credits_id_seq', 10, true);


--
-- Data for Name: user_identity; Type: TABLE DATA; Schema: public; Owner: degree
--

COPY user_identity (id, name) FROM stdin;
1	Jamppa
2	Mirja
3	Seppo
\.


--
-- Data for Name: user_plan; Type: TABLE DATA; Schema: public; Owner: degree
--

COPY user_plan (id, id_user, id_studycomponent) FROM stdin;
1	1	1
3	1	79
\.


--
-- Name: user_plan_id_seq; Type: SEQUENCE SET; Schema: public; Owner: degree
--

SELECT pg_catalog.setval('user_plan_id_seq', 3, true);


--
-- Data for Name: user_selected_studycomponent; Type: TABLE DATA; Schema: public; Owner: degree
--

COPY user_selected_studycomponent (id, id_user, id_studycomponent) FROM stdin;
1	1	6
2	1	7
3	1	8
4	1	9
5	1	12
7	1	27
8	1	30
9	1	33
10	1	64
12	1	70
\.


--
-- Name: user_selected_studycomponent_id_seq; Type: SEQUENCE SET; Schema: public; Owner: degree
--

SELECT pg_catalog.setval('user_selected_studycomponent_id_seq', 12, true);


--
-- Name: idea_pkey; Type: CONSTRAINT; Schema: public; Owner: degree; Tablespace: 
--

ALTER TABLE ONLY idea
    ADD CONSTRAINT idea_pkey PRIMARY KEY (id);


--
-- Name: studycomponent_inclusion_pkey; Type: CONSTRAINT; Schema: public; Owner: degree; Tablespace: 
--

ALTER TABLE ONLY studycomponent_inclusion
    ADD CONSTRAINT studycomponent_inclusion_pkey PRIMARY KEY (id);


--
-- Name: studycomponent_pkey; Type: CONSTRAINT; Schema: public; Owner: degree; Tablespace: 
--

ALTER TABLE ONLY studycomponent
    ADD CONSTRAINT studycomponent_pkey PRIMARY KEY (id);


--
-- Name: studycomponent_type_pkey; Type: CONSTRAINT; Schema: public; Owner: degree; Tablespace: 
--

ALTER TABLE ONLY studycomponent_type
    ADD CONSTRAINT studycomponent_type_pkey PRIMARY KEY (id);


--
-- Name: uniq_child_per_parent_component; Type: CONSTRAINT; Schema: public; Owner: degree; Tablespace: 
--

ALTER TABLE ONLY studycomponent_inclusion
    ADD CONSTRAINT uniq_child_per_parent_component UNIQUE (parent, child);


--
-- Name: uniq_component_lang_text; Type: CONSTRAINT; Schema: public; Owner: degree; Tablespace: 
--

ALTER TABLE ONLY studycomponent_text
    ADD CONSTRAINT uniq_component_lang_text UNIQUE (id_studycomponent, lang);


--
-- Name: uniq_ext_id; Type: CONSTRAINT; Schema: public; Owner: degree; Tablespace: 
--

ALTER TABLE ONLY studycomponent
    ADD CONSTRAINT uniq_ext_id UNIQUE (externalid);


--
-- Name: uniq_user_sc_select; Type: CONSTRAINT; Schema: public; Owner: degree; Tablespace: 
--

ALTER TABLE ONLY user_selected_studycomponent
    ADD CONSTRAINT uniq_user_sc_select UNIQUE (id_user, id_studycomponent);


--
-- Name: user_component_mount_pkey; Type: CONSTRAINT; Schema: public; Owner: degree; Tablespace: 
--

ALTER TABLE ONLY user_component_mount
    ADD CONSTRAINT user_component_mount_pkey PRIMARY KEY (id);


--
-- Name: user_credits_pkey; Type: CONSTRAINT; Schema: public; Owner: degree; Tablespace: 
--

ALTER TABLE ONLY user_credits
    ADD CONSTRAINT user_credits_pkey PRIMARY KEY (id);


--
-- Name: user_identity_pkey; Type: CONSTRAINT; Schema: public; Owner: degree; Tablespace: 
--

ALTER TABLE ONLY user_identity
    ADD CONSTRAINT user_identity_pkey PRIMARY KEY (id);


--
-- Name: user_plan_id_user_id_studycomponent_key; Type: CONSTRAINT; Schema: public; Owner: degree; Tablespace: 
--

ALTER TABLE ONLY user_plan
    ADD CONSTRAINT user_plan_id_user_id_studycomponent_key UNIQUE (id_user, id_studycomponent);


--
-- Name: user_plan_pkey; Type: CONSTRAINT; Schema: public; Owner: degree; Tablespace: 
--

ALTER TABLE ONLY user_plan
    ADD CONSTRAINT user_plan_pkey PRIMARY KEY (id);


--
-- Name: user_selected_studycomponent_pkey; Type: CONSTRAINT; Schema: public; Owner: degree; Tablespace: 
--

ALTER TABLE ONLY user_selected_studycomponent
    ADD CONSTRAINT user_selected_studycomponent_pkey PRIMARY KEY (id);


--
-- Name: studycomponent_inclusion_child_fkey; Type: FK CONSTRAINT; Schema: public; Owner: degree
--

ALTER TABLE ONLY studycomponent_inclusion
    ADD CONSTRAINT studycomponent_inclusion_child_fkey FOREIGN KEY (child) REFERENCES studycomponent(id);


--
-- Name: studycomponent_inclusion_parent_fkey; Type: FK CONSTRAINT; Schema: public; Owner: degree
--

ALTER TABLE ONLY studycomponent_inclusion
    ADD CONSTRAINT studycomponent_inclusion_parent_fkey FOREIGN KEY (parent) REFERENCES studycomponent(id);


--
-- Name: studycomponent_sc_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: degree
--

ALTER TABLE ONLY studycomponent
    ADD CONSTRAINT studycomponent_sc_type_fkey FOREIGN KEY (sc_type) REFERENCES studycomponent_type(id);


--
-- Name: studycomponent_text_id_studycomponent_fkey; Type: FK CONSTRAINT; Schema: public; Owner: degree
--

ALTER TABLE ONLY studycomponent_text
    ADD CONSTRAINT studycomponent_text_id_studycomponent_fkey FOREIGN KEY (id_studycomponent) REFERENCES studycomponent(id);


--
-- Name: user_component_mount_id_component_what_fkey; Type: FK CONSTRAINT; Schema: public; Owner: degree
--

ALTER TABLE ONLY user_component_mount
    ADD CONSTRAINT user_component_mount_id_component_what_fkey FOREIGN KEY (id_component_what) REFERENCES studycomponent(id);


--
-- Name: user_component_mount_id_component_where_fkey; Type: FK CONSTRAINT; Schema: public; Owner: degree
--

ALTER TABLE ONLY user_component_mount
    ADD CONSTRAINT user_component_mount_id_component_where_fkey FOREIGN KEY (id_component_where) REFERENCES studycomponent(id);


--
-- Name: user_component_mount_id_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: degree
--

ALTER TABLE ONLY user_component_mount
    ADD CONSTRAINT user_component_mount_id_user_fkey FOREIGN KEY (id_user) REFERENCES user_identity(id);


--
-- Name: user_credits_id_idea_fkey; Type: FK CONSTRAINT; Schema: public; Owner: degree
--

ALTER TABLE ONLY user_credits
    ADD CONSTRAINT user_credits_id_idea_fkey FOREIGN KEY (id_idea) REFERENCES idea(id);


--
-- Name: user_credits_id_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: degree
--

ALTER TABLE ONLY user_credits
    ADD CONSTRAINT user_credits_id_user_fkey FOREIGN KEY (id_user) REFERENCES user_identity(id);


--
-- Name: user_plan_id_studycomponent_fkey; Type: FK CONSTRAINT; Schema: public; Owner: degree
--

ALTER TABLE ONLY user_plan
    ADD CONSTRAINT user_plan_id_studycomponent_fkey FOREIGN KEY (id_studycomponent) REFERENCES studycomponent(id);


--
-- Name: user_plan_id_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: degree
--

ALTER TABLE ONLY user_plan
    ADD CONSTRAINT user_plan_id_user_fkey FOREIGN KEY (id_user) REFERENCES user_identity(id);


--
-- Name: user_selected_studycomponent_id_studycomponent_fkey; Type: FK CONSTRAINT; Schema: public; Owner: degree
--

ALTER TABLE ONLY user_selected_studycomponent
    ADD CONSTRAINT user_selected_studycomponent_id_studycomponent_fkey FOREIGN KEY (id_studycomponent) REFERENCES studycomponent(id);


--
-- Name: user_selected_studycomponent_id_user_fkey; Type: FK CONSTRAINT; Schema: public; Owner: degree
--

ALTER TABLE ONLY user_selected_studycomponent
    ADD CONSTRAINT user_selected_studycomponent_id_user_fkey FOREIGN KEY (id_user) REFERENCES user_identity(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

